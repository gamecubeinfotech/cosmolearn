import 'dart:convert';

import 'package:cosmolearn/appUtilities/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:razorpay_flutter/razorpay_flutter.dart';
import '../appUtilities/AppPrefrences.dart';
import '../appUtilities/app_circular_loader.dart';
import '../appUtilities/utils.dart';
import '../view_models/buy_models.dart';

class BuyNowPage extends StatefulWidget {
  String price;
  String receiptId;
  String bookDes;
  String bookImage;
  BuyNowPage(this.price, this.receiptId, this.bookDes, this.bookImage, {super.key});

  @override
  State<BuyNowPage> createState() => _BuyNowPageState();
}

class _BuyNowPageState extends State<BuyNowPage> {
  String orderID = "";

  Razorpay? _razorpay;

  @override
  void initState() {
    super.initState();
    print("price ${widget.price}");
    print("receiptId ${widget.receiptId}");
    print("bookDes ${widget.bookDes}");
    print("bookImage ${widget.bookImage}");
    Future.delayed(Duration.zero, () {
      getRazorpayId();

    });

    _razorpay = Razorpay();
    _razorpay!.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay!.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay!.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
  }

  @override
  void dispose() {
    super.dispose();
    _razorpay!.clear();
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    print("Payment Success: ${response.paymentId}");
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    print("Payment Error: ${response.code} - ${response.message}");
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    print("External Wallet: ${response.walletName}");
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: mainColor,
      body: Container(),
    );
  }

  Future<void> getRazorpayId() async {

    AppLoaderProgress.showLoader(context);
    DateTime currentTime = DateTime.now();
    int milliseconds = currentTime.millisecond;
    String clientId = await AppPrefrence.getString('id');
    print("clientId ${widget.receiptId}$clientId${milliseconds.toString()}");

    final response = await http.post(
      Uri.parse(UtilsMethod.baseUrl),
      body: {
        "api_name": "getRazorpayOrderId",
        "amount": widget.price,
        "receiptId": "${widget.receiptId}$clientId${milliseconds.toString()}",
      },
    );
    print("response ${response.statusCode}");
    if (response.statusCode == 200) {


      orderID = BuyNowModels.fromJson(jsonDecode(response.body)).resonse!.result!;

      Navigator.pop(context);

      startPayment();
      setState(() {});
    }

    AppLoaderProgress.hideLoader(context);
  }

  void startPayment() async {
    String mobileNumber = await AppPrefrence.getString("mobile");
    String emailID = await AppPrefrence.getString('emailId');

    var options = {
      'key': 'rzp_live_NVc7iDHJckZDPG',
      'name': 'CosmoLearn',
      'amount': widget.price,
      'description': widget.bookDes,
      'image': widget.bookImage,
      'order_id': orderID,
      'theme.color': '#3399cc',
      'currency': 'INR',
      'prefill.email': emailID,
      'prefill.contact': mobileNumber,

    };

    try {
      _razorpay!.open(options);
    } catch (e) {
      print('Error: $e');
    }
  }
}
