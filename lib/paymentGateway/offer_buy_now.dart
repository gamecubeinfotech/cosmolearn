import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:http/http.dart' as http;
import '../appUtilities/AppPrefrences.dart';
import '../appUtilities/app_circular_loader.dart';
import '../appUtilities/app_colors.dart';
import '../appUtilities/utils.dart';
import '../view_models/buy_models.dart';


class OfferBuyNow extends StatefulWidget {
  String? amount;
  String? offerId;
  String? offDescription;
  OfferBuyNow(this.amount,this.offerId,this.offDescription);

  @override
  State<OfferBuyNow> createState() => _OfferBuyNowState();
}

class _OfferBuyNowState extends State<OfferBuyNow> {
  String orderID = "";

  Razorpay? _razorpay;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      getRazorpayOfferId();

    });

    _razorpay = Razorpay();
    _razorpay!.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay!.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay!.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
  }

  @override
  void dispose() {
    super.dispose();
    _razorpay!.clear();
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    print("Payment Success: ${response.paymentId}");
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    print("Payment Error: ${response.code} - ${response.message}");
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    print("External Wallet: ${response.walletName}");
  }


  Widget build(BuildContext context) {
    return Scaffold(backgroundColor: mainColor,body: Container());
  }

  Future<void> getRazorpayOfferId() async {
    AppLoaderProgress.showLoader(context);

    final response = await http.post(
      Uri.parse(UtilsMethod.baseUrl),
      body: {
        "api_name": "getRazorpayOrderId",
        "amount": widget.amount,
        "course_id": "0",
        "test_series_id": "0",
        "offer_id": widget.offerId,
      },
    );
    if (response.statusCode == 200) {
      orderID = BuyNowModels.fromJson(jsonDecode(response.body)).resonse!.result!;
      startOfferPayment();
      Navigator.pop(context);
      setState(() {});
    }

    AppLoaderProgress.hideLoader(context);
  }

  void startOfferPayment() async {

    String mobileNumber = await AppPrefrence.getString("mobile");
    String emailID = await AppPrefrence.getString('emailId');

    var options = {
      'key': 'rzp_live_NVc7iDHJckZDPG',
      'name': 'CosmoLearn',
      'amount': widget.amount!.split("/-")[0],
      'description': widget.offDescription,
      'image': "",
      'order_id': orderID,
      'theme.color': '#3399cc',
      'currency': 'INR',
      'prefill.email': emailID,
      'prefill.contact': mobileNumber,

    };

    try {
      _razorpay!.open(options);
    } catch (e) {
      if (kDebugMode) {
        print('Error: $e');
      }
    }
  }

}
