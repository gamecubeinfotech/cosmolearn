import 'package:flutter/cupertino.dart';

class CarouselIndexProvider extends ChangeNotifier {
  int _carouselCurrentIndex = 0;

  int get currentIndex => _carouselCurrentIndex;

  void updateIndex(int index) {
    _carouselCurrentIndex = index;
    notifyListeners();
  }
}
