class RoutesName {
  //Names should be unique
  static const String splash = "splash_screen";
  static const String home = "home_screen";
  static const String login = "login_screen";
  static const String signUp = "signup_screen";
  static const String otpScreen = "otp_screen";
  static const String testSeries = "test_series";
  static const String testSeriesDetail = "Test_series_detail";
  static const String offers = "Offers";
  static const String report = "Report";
  static const String contactUs = "Contact Us";
  static const String aboutUs = "About Us";
  static const String courseDetail = "Course Details";
  static const String courseStart = "Course Start";
  static const String allQuestionList = "All Question List";
  static const String startTest = "Start Test";
  static const String review = "Review";
  static const String submit = "Submit";

}