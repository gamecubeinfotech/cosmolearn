import 'package:cosmolearn/appUtilities/routes/routes_name.dart';
import 'package:cosmolearn/screen/test_series.dart';
import 'package:flutter/material.dart';
import '../../screen/about_us.dart';
import '../../screen/all_question_list.dart';
import '../../screen/contact.dart';
import '../../screen/course_details.dart';
import '../../screen/course_start.dart';
import '../../screen/home.dart';
import '../../screen/login_screen.dart';
import '../../screen/offer.dart';
import '../../screen/otp_screen.dart';
import '../../screen/report.dart';
import '../../screen/review.dart';
import '../../screen/sign_up.dart';
import '../../screen/splash_view.dart';
import '../../screen/start_test.dart';
import '../../screen/test_series_details.dart';

class Routes {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final arguments = (settings.arguments ?? <String, dynamic>{}) as Map;
    switch (settings.name) {
      case RoutesName.home:
        return MaterialPageRoute(
          builder: (BuildContext context) => HomeScreen(),
        );
      case RoutesName.splash:
        return MaterialPageRoute(
          builder: (BuildContext context) => SplashView(),
        );
      case RoutesName.login:
        return MaterialPageRoute(
          builder: (BuildContext context) => LoginScreen(),
        );
      case RoutesName.signUp:
        return MaterialPageRoute(
          builder: (BuildContext context) => SignUp(),
        );
      case RoutesName.otpScreen:
        return MaterialPageRoute(
          builder: (BuildContext context) => otpScreen(
            arguments['otpText'],
          ),
        );
      case RoutesName.testSeries:
        return MaterialPageRoute(
          builder: (BuildContext context) => TestSeries(),
        );
      case RoutesName.testSeriesDetail:
        return MaterialPageRoute(
          builder: (BuildContext context) => TestSeriesDetails(
            arguments['testtype'],
          ),
        );
      case RoutesName.offers:
        return MaterialPageRoute(
          builder: (BuildContext context) => Offer(),
        );
      case RoutesName.report:
        return MaterialPageRoute(
          builder: (BuildContext context) => Report(),
        );
      case RoutesName.contactUs:
        return MaterialPageRoute(
          builder: (BuildContext context) => Contact(),
        );
      case RoutesName.aboutUs:
        return MaterialPageRoute(
          builder: (BuildContext context) => AboutUs(),
        );
      case RoutesName.courseDetail:
        return MaterialPageRoute(
          builder: (BuildContext context) => CourseDetails(arguments['bookName']),
        );
      case RoutesName.courseStart:
        return MaterialPageRoute(
          builder: (BuildContext context) => CourseStart(arguments['bookId'], arguments['ChapterId'],arguments['ChapterName'],arguments['ChapterNotes'],arguments['ChapterVideo']),
        );
      // case RoutesName.startTest:
      //   return MaterialPageRoute(
      //     builder: (BuildContext context) => StartTest(arguments['TypeTestName'],arguments['TypeTestid']),
      //   );
        // case RoutesName.review:
        // return MaterialPageRoute(
        //   builder: (BuildContext context) => ReviewScreen( reviewedQuestions: [],),
        // );

      case RoutesName.allQuestionList:
        return MaterialPageRoute(
          builder: (BuildContext context) => AllQuestionList(
            arguments['id'],
            arguments['type'],
          ),
        );

      default:
        return MaterialPageRoute(
            builder: (BuildContext context) => const Scaffold(
                  body: Center(
                    child: Text("No route defined"),
                  ),
                ));
    }
  }
}
