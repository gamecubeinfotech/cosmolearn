import 'package:flutter/material.dart';

class AppConstants{


  static const String submit = 'Submit';
  static const String resetPassword = 'Reset Password';
  static const String createAccount = 'Create Account';
  static const String Continue = 'Continue';
  static const String createNewPassword = 'Create New Password';
  static const String password = 'Password';
  static const String enterPassword = '   Enter your password';
  static const String confirmPassworld = 'Confirm Password';

  //------------------------------------------------------------------------
  static const String loginPageText = 'We happy to see you here again. Enter your mobile number.';
  static const String enterMobile = 'Mobile Number';
  static const String otpSent = 'We will send you OTP.';
  static const String sign_Up = 'Sign Up';
  static const String welcome = 'Welcome,';
  static const String login = 'Login';
  //------------------------------------------------------------------------
  static const String enterName = 'Enter your name';
  static const String enterYourEmail = 'Enter your email address';
  //------------------------------------------------------------------------
  static const String verifyOtp = 'Verify OTP';
  static const String verify = 'Verify';
  static const String verifyOtpText = 'Enter the 4 digit number that we send to +91-**********';
  //------------------------------------------------------------------------
  static const String discoverTest = 'Discover Test Series';
  static const String viewAll = 'View All';
  static const String biology = 'Biology';
  static const String anatomy = 'Anatomy SWT online test series';
  static const String surgeryText = 'Surgery/ Dr. Saurabh gupta';
  static const String nextPattern = 'Next pattern test';
  static const String rupeesSymbol = '₹';
  static const String coursesOfferred = 'Courses Offerred';
  static const String offer = '30% off';
  static const String questions = 'Questions';
  static const String buy = 'Buy';
  static const String tryNow = 'Try';
  static const String description = 'Buy 4 test of Anatomy, 50 MCQ in each test, get result, get explanation.';
  static const String score = 'Score';
  static const String attempted = 'Attempted: ';
  static const String marks = 'Marks: ';
  static const String correct = 'Correct';
  static const String wrong = 'Wrong';
  static const String skipped = 'Skipped';
  static const String all = 'All';
  //----------------------------------------------------------------------------------------------------------------
  static const String contactUs = 'Contact Us';
  static const String supportMail = 'Support mail';
  static const String saleMail = 'Sales mail';
  static const String contactMail = 'Contact mail';
  static const String cosmolearnMail = 'support@nayiurja.com';
  static const String cosmolearnSalesMail = 'sales@nayiurja.com';
  static const String cosmolearnContactMail = 'contactus@nayiurja.com';
//----------------------------------------------------------------------------------------------------------------
  static const String aboutUS = 'About Us';
  static const String aboutUsDescription = "Welcome to CosmoLearn, your ultimate destination for an exceptional online test series experience! ";
  static const String aboutUsDescription1 = "We are dedicated to empowering learners of all ages and backgrounds to reach their full potential by providing a comprehensive platform to ace examinations with confidence.";
  static const String aboutUsDescription2 = "Our user-friendly app offers a wide range of meticulously curated test series, tailored to suit various competitive exams and academic subjects. With cutting-edge technology and insightful analytics, CosmoLearn ensures personalized learning pathways, enabling you to identify your strengths and weaknesses, thus optimizing your performance.";
  static const String aboutUsDescription3 = "Join us on this transformative journey, where knowledge meets innovation, and together, we'll unlock a world of limitless possibilities. Your success is our priority, and at CosmoLearn, we stand committed to supporting you every step of the way!";

//----------------------------------------------------------------------------------------------------------------

  static const String video = 'Video';
  static const String questionBank = 'Question Bank';
  static const String notes = 'Notes';
  static const String open = 'Open';
  static const String skip = 'SKIP';
  static const String review = 'REVIEW';
  static const String next = 'Next';
  static const String report = 'Report';
  static const String submitButton = 'SUBMIT';
  static const String explanation  = 'Explanation: ';
  static const String answerExplanation  = 'Answer Explanation: ';
  static const String newVersion  = 'A New Version Is Available';
  static const String versionDescription  = 'For more features and a better user experience, Please upgrade this app.';
  static const String startTest = 'Start Test';
}