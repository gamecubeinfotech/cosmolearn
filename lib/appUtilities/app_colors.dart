import 'package:flutter/material.dart';




Color get primaryColor => const Color(0xff2A2A2A);
Color get mainColor => const Color(0xffF6F6F6);
Color get whiteColor => const Color(0xffffffff);
Color get blackColor => const Color(0xff000000);
Color get lightBlue => const Color(0xff196BFF);
Color get lightRed => const Color(0xffFB7C47);
Color get subText => const Color(0xff8491A1);
Color get textField => const Color(0xffB5B9C3);
Color get borderColor => const Color(0xffEBEEF2);
Color get lightSky => const Color(0xffE1F3FF);
Color get brightRed => const Color(0xffFFE2D6);
Color get greenColor => Colors.green.shade300;
Color get redColor => Colors.red.shade300;
Color get colorWhiteLowEmp => const Color(0xffE8E8E8);