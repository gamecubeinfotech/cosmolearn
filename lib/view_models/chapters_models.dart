class ChaptersModeals {
  ChaptersResonse? resonse;

  ChaptersModeals({this.resonse});

  ChaptersModeals.fromJson(Map<String, dynamic> json) {
    resonse =
    json['resonse'] != null ? new ChaptersResonse.fromJson(json['resonse']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.resonse != null) {
      data['resonse'] = this.resonse!.toJson();
    }
    return data;
  }
}

class ChaptersResonse {
  int? status;
  List<ChaptersResult>? result;

  ChaptersResonse({this.status, this.result});

  ChaptersResonse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['result'] != null) {
      result = <ChaptersResult>[];
      json['result'].forEach((v) {
        result!.add(new ChaptersResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.result != null) {
      data['result'] = this.result!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ChaptersResult {
  String? chapterId;
  String? chapterName;
  String? chapterDes;
  String? chapterImage;
  String? chapterVideo;
  String? chapterNotes;
  String? bookID;
  String? lockStatus;

  ChaptersResult(
      {this.chapterId,
        this.chapterName,
        this.chapterDes,
        this.chapterImage,
        this.chapterVideo,
        this.chapterNotes,
        this.bookID,
        this.lockStatus});

  ChaptersResult.fromJson(Map<String, dynamic> json) {
    chapterId = json['Chapter_Id'];
    chapterName = json['Chapter_Name'];
    chapterDes = json['Chapter_des'];
    chapterImage = json['Chapter_Image'];
    chapterVideo = json['Chapter_Video'];
    chapterNotes = json['ChapterNotes'];
    bookID = json['BookID'];
    lockStatus = json['lock_status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Chapter_Id'] = this.chapterId;
    data['Chapter_Name'] = this.chapterName;
    data['Chapter_des'] = this.chapterDes;
    data['Chapter_Image'] = this.chapterImage;
    data['Chapter_Video'] = this.chapterVideo;
    data['ChapterNotes'] = this.chapterNotes;
    data['BookID'] = this.bookID;
    data['lock_status'] = this.lockStatus;
    return data;
  }
}



