import 'dart:convert';

import 'package:flutter/material.dart';

class AddQuestionModels{
  AddQuestionModels();
  String? book_id; //-- 0;
  String? chapter_id;  //-- 0;
  String? user_mobile;
  String? question_number;
  String? answer_marked;
  String? correct_answer;
  String? check_status; //-- markedAnswer.equalsIgnoreCase(model.getCorrect_answer())?"true":"false";
  String? time_taken_to_solve; //-- 0s
  String? flag;  //-- ATTEMPTED / NOT_ATTEMPTED;
  String? ttname_id;
  String? is_submit; //-- "NO"
  String? client_id;
  String? q_name;

  AddQuestionModels.fromJson(Map<String, dynamic> json) {
    book_id = json['book_id'];
    chapter_id = json['chapter_id'];
    user_mobile = json['user_mobile'];
    question_number = json['question_number'];
    answer_marked = json['answer_marked'];
    correct_answer = json['correct_answer'];
    check_status = json['check_status'];
    time_taken_to_solve = json['time_taken_to_solve'];
    flag = json['flag'];
    ttname_id = json['ttname_id'];
    is_submit = json['is_submit'];
    client_id = json['client_id'];
    q_name = json['q_name'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['book_id'] = book_id;
    data['chapter_id'] = chapter_id;
    data['user_mobile'] = user_mobile;
    data['question_number'] = question_number;
    data['answer_marked'] = answer_marked;
    data['correct_answer'] = correct_answer;
    data['check_status'] = check_status;
    data['time_taken_to_solve'] = time_taken_to_solve;
    data['flag'] = flag;
    data['ttname_id'] = ttname_id;
    data['is_submit'] = is_submit;
    data['client_id'] = client_id;
    data['q_name'] = q_name;
    return data;
  }
}