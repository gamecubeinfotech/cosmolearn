class QuestionDataModels {
  QuestionDataResonse? resonse;

  QuestionDataModels({this.resonse});

  QuestionDataModels.fromJson(Map<String, dynamic> json) {
    resonse =
    json['resonse'] != null ? new QuestionDataResonse.fromJson(json['resonse']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.resonse != null) {
      data['resonse'] = this.resonse!.toJson();
    }
    return data;
  }
}

class QuestionDataResonse {
  int? status;
  List<QuestionDataResult>? result;

  QuestionDataResonse({this.status, this.result});

  QuestionDataResonse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['result'] != null) {
      result = <QuestionDataResult>[];
      json['result'].forEach((v) {
        result!.add(new QuestionDataResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.result != null) {
      data['result'] = this.result!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class QuestionDataResult {
  String? id;
  String? ttnameId;
  String? qNo;
  String? bookId;
  String? chapterId;
  String? question;
  String? optionA;
  String? optionB;
  String? optionC;
  String? optionD;
  String? optionAImage;
  String? optionBImage;
  String? optionCImage;
  String? optionDImage;
  String? questionImage;
  String? explaination;
  String? imageExplain;
  String? correctAnswer;
  String? aunswerImage;
  String? answerMarked;
  String? timeTakenToSolve;
  String? isAttempted;

  QuestionDataResult(
      {this.id,
        this.ttnameId,
        this.qNo,
        this.bookId,
        this.chapterId,
        this.question,
        this.optionA,
        this.optionB,
        this.optionC,
        this.optionD,
        this.optionAImage,
        this.optionBImage,
        this.optionCImage,
        this.optionDImage,
        this.questionImage,
        this.explaination,
        this.imageExplain,
        this.correctAnswer,
        this.aunswerImage,
        this.answerMarked,
        this.timeTakenToSolve,
        this.isAttempted});

  QuestionDataResult.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    ttnameId = json['ttname_id'];
    qNo = json['q_no'];
    bookId = json['book_id'];
    chapterId = json['chapter_id'];
    question = json['question'];
    optionA = json['option_a'];
    optionB = json['option_b'];
    optionC = json['option_c'];
    optionD = json['option_d'];
    optionAImage = json['option_a_image'];
    optionBImage = json['option_b_image'];
    optionCImage = json['option_c_image'];
    optionDImage = json['option_d_image'];
    questionImage = json['question_image'];
    explaination = json['explaination'];
    imageExplain = json['image_explain'];
    correctAnswer = json['correct_answer'];
    aunswerImage = json['aunswer_image'];
    answerMarked = json['answer_marked'];
    timeTakenToSolve = json['time_taken_to_solve'];
    isAttempted = json['is_attempted'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['ttname_id'] = this.ttnameId;
    data['q_no'] = this.qNo;
    data['book_id'] = this.bookId;
    data['chapter_id'] = this.chapterId;
    data['question'] = this.question;
    data['option_a'] = this.optionA;
    data['option_b'] = this.optionB;
    data['option_c'] = this.optionC;
    data['option_d'] = this.optionD;
    data['option_a_image'] = this.optionAImage;
    data['option_b_image'] = this.optionBImage;
    data['option_c_image'] = this.optionCImage;
    data['option_d_image'] = this.optionDImage;
    data['question_image'] = this.questionImage;
    data['explaination'] = this.explaination;
    data['image_explain'] = this.imageExplain;
    data['correct_answer'] = this.correctAnswer;
    data['aunswer_image'] = this.aunswerImage;
    data['answer_marked'] = this.answerMarked;
    data['time_taken_to_solve'] = this.timeTakenToSolve;
    data['is_attempted'] = this.isAttempted;
    return data;
  }
}
