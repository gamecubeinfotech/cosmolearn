class BannerModels {
  BannerResonse? resonse;

  BannerModels({this.resonse});

  BannerModels.fromJson(Map<String, dynamic> json) {
    resonse =
    json['resonse'] != null ? new BannerResonse.fromJson(json['resonse']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.resonse != null) {
      data['resonse'] = this.resonse!.toJson();
    }
    return data;
  }
}

class BannerResonse {
  int? status;
  List<BannerResult>? result;

  BannerResonse({this.status, this.result});

  BannerResonse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['result'] != null) {
      result = <BannerResult>[];
      json['result'].forEach((v) {
        result!.add(BannerResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.result != null) {
      data['result'] = this.result!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class BannerResult {
  String? id;
  String? bannerImage;

  BannerResult({this.id, this.bannerImage});

  BannerResult.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    bannerImage = json['banner_image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['banner_image'] = this.bannerImage;
    return data;
  }
}
