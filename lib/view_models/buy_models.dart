class BuyNowModels {
  BuyNowResonse? resonse;

  BuyNowModels({this.resonse});

  BuyNowModels.fromJson(Map<String, dynamic> json) {
    resonse =
    json['resonse'] != null ? new BuyNowResonse.fromJson(json['resonse']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.resonse != null) {
      data['resonse'] = this.resonse!.toJson();
    }
    return data;
  }
}

class BuyNowResonse {
  int? status;
  String? result;

  BuyNowResonse({this.status, this.result});

  BuyNowResonse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['result'] = this.result;
    return data;
  }
}
