

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../appUtilities/AppPrefrences.dart';
import '../appUtilities/routes/routes_name.dart';
import '../screen/home.dart';
import '../screen/login_screen.dart';


class SplashServices {
  //Trying to get the data which we stored as Shared Preferences
  //Getter method of UserViewModel


  void checkAuthentication(BuildContext context) async {
    await Future.delayed(const Duration(seconds: 3));

    AppPrefrence.getBoolean("isLogin").then((value) {
      if (value) {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => HomeScreen(),),
              (route) => false,
        );
      } else {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => LoginScreen(),),
              (route) => false,
        );
      }
    });
    // if (checkAuthentication == "null" || checkAuthentication.toString() == "") {
    //   await Future.delayed(Duration(seconds: 3));
    //   Navigator.pushNamed(context, RoutesName.mainScreen);
    //
    // } else {
    //   await Future.delayed(Duration(seconds: 3));
    //   Navigator.pushNamed(context, RoutesName.home);
    // }
    }
}
