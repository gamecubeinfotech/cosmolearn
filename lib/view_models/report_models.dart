class ReportModels {
  ReportResonse? resonse;

  ReportModels({this.resonse});

  ReportModels.fromJson(Map<String, dynamic> json) {
    resonse =
        json['resonse'] != null ? ReportResonse.fromJson(json['resonse']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.resonse != null) {
      data['resonse'] = this.resonse!.toJson();
    }
    return data;
  }
}

class ReportResonse {
  int? status;
  List<ReportResult>? result;

  ReportResonse({this.status, this.result});

  ReportResonse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['result'] != null) {
      result = <ReportResult>[];
      json['result'].forEach((v) {
        result!.add(new ReportResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.result != null) {
      data['result'] = this.result!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ReportResult {
  String? id;
  String? clientId;
  String? ttnameId;
  String? attemptedCount;
  String? negMarkQCount;
  String? totalCount;
  String? marksOfQuestion;
  String? negativeMarks;
  String? totalMarks;
  String? testName;
  String? markes;
  String? percentage;
  String? rank;
  String? skipQuestionCount;
  String? correctQuestionCount;
  String? totalRank;
  String? explaination;
  String? created;
  String? modified;

  ReportResult(
      {this.id,
      this.clientId,
      this.ttnameId,
      this.attemptedCount,
      this.negMarkQCount,
      this.totalCount,
      this.marksOfQuestion,
      this.negativeMarks,
      this.totalMarks,
      this.testName,
      this.markes,
      this.percentage,
      this.rank,
      this.skipQuestionCount,
      this.correctQuestionCount,
      this.totalRank,
      this.explaination,
      this.created,
      this.modified});

  ReportResult.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    clientId = json['client_id'];
    ttnameId = json['ttname_id'];
    attemptedCount = json['attempted_count'];
    negMarkQCount = json['neg_mark_q_count'];
    totalCount = json['total_count'];
    marksOfQuestion = json['marks_of_question'];
    negativeMarks = json['negative_marks'];
    totalMarks = json['total_marks'];
    testName = json['test_name'];
    markes = json['markes'];
    percentage = json['percentage'];
    rank = json['rank'];
    skipQuestionCount = json['skip_question_count'];
    correctQuestionCount = json['correct_question_count'];
    totalRank = json['total_rank'];
    explaination = json['explaination'];
    created = json['created'];
    modified = json['modified'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['client_id'] = this.clientId;
    data['ttname_id'] = this.ttnameId;
    data['attempted_count'] = this.attemptedCount;
    data['neg_mark_q_count'] = this.negMarkQCount;
    data['total_count'] = this.totalCount;
    data['marks_of_question'] = this.marksOfQuestion;
    data['negative_marks'] = this.negativeMarks;
    data['total_marks'] = this.totalMarks;
    data['test_name'] = this.testName;
    data['markes'] = this.markes;
    data['percentage'] = this.percentage;
    data['rank'] = this.rank;
    data['skip_question_count'] = this.skipQuestionCount;
    data['correct_question_count'] = this.correctQuestionCount;
    data['total_rank'] = this.totalRank;
    data['explaination'] = this.explaination;
    data['created'] = this.created;
    data['modified'] = this.modified;
    return data;
  }
}
