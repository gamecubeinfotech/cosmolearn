class QuestionModeals {
  QuestionResonse? resonse;

  QuestionModeals({this.resonse});

  QuestionModeals.fromJson(Map<String, dynamic> json) {
    resonse =
    json['resonse'] != null ? new QuestionResonse.fromJson(json['resonse']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.resonse != null) {
      data['resonse'] = this.resonse!.toJson();
    }
    return data;
  }
}

class QuestionResonse {
  int? status;
  List<QuestionResult>? result;

  QuestionResonse({this.status, this.result});

  QuestionResonse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['result'] != null) {
      result = <QuestionResult>[];
      json['result'].forEach((v) {
        result!.add(new QuestionResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.result != null) {
      data['result'] = this.result!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class QuestionResult {
  String? id;
  String? ttnameId;
  String? qNo;
  String? bookId;
  String? chapterId;
  String? question;
  String? optionA;
  String? optionB;
  String? optionC;
  String? optionD;
  String? optionAImage;
  String? optionBImage;
  String? optionCImage;
  String? optionDImage;
  String? questionImage;
  String? explaination;
  String? imageExplain;
  String? correctAnswer;
  String? aunswerImage;
  String? diffIndex;
  String? created;
  String? modified;

  QuestionResult(
      {this.id,
        this.ttnameId,
        this.qNo,
        this.bookId,
        this.chapterId,
        this.question,
        this.optionA,
        this.optionB,
        this.optionC,
        this.optionD,
        this.optionAImage,
        this.optionBImage,
        this.optionCImage,
        this.optionDImage,
        this.questionImage,
        this.explaination,
        this.imageExplain,
        this.correctAnswer,
        this.aunswerImage,
        this.diffIndex,
        this.created,
        this.modified});

  QuestionResult.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    ttnameId = json['ttname_id'];
    qNo = json['q_no'];
    bookId = json['book_id'];
    chapterId = json['chapter_id'];
    question = json['question'];
    optionA = json['option_a'];
    optionB = json['option_b'];
    optionC = json['option_c'];
    optionD = json['option_d'];
    optionAImage = json['option_a_image'];
    optionBImage = json['option_b_image'];
    optionCImage = json['option_c_image'];
    optionDImage = json['option_d_image'];
    questionImage = json['question_image'];
    explaination = json['explaination'];
    imageExplain = json['image_explain'];
    correctAnswer = json['correct_answer'];
    aunswerImage = json['aunswer_image'];
    diffIndex = json['diff_index'];
    created = json['created'];
    modified = json['modified'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['ttname_id'] = this.ttnameId;
    data['q_no'] = this.qNo;
    data['book_id'] = this.bookId;
    data['chapter_id'] = this.chapterId;
    data['question'] = this.question;
    data['option_a'] = this.optionA;
    data['option_b'] = this.optionB;
    data['option_c'] = this.optionC;
    data['option_d'] = this.optionD;
    data['option_a_image'] = this.optionAImage;
    data['option_b_image'] = this.optionBImage;
    data['option_c_image'] = this.optionCImage;
    data['option_d_image'] = this.optionDImage;
    data['question_image'] = this.questionImage;
    data['explaination'] = this.explaination;
    data['image_explain'] = this.imageExplain;
    data['correct_answer'] = this.correctAnswer;
    data['aunswer_image'] = this.aunswerImage;
    data['diff_index'] = this.diffIndex;
    data['created'] = this.created;
    data['modified'] = this.modified;
    return data;
  }
}
