// class CourseModels {
//   CourseResonse? resonse;
//
//   CourseModels({this.resonse});
//
//   CourseModels.fromJson(Map<String, dynamic> json) {
//     resonse =
//     json['resonse'] != null ? new CourseResonse.fromJson(json['resonse']) : null;
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     if (this.resonse != null) {
//       data['resonse'] = this.resonse!.toJson();
//     }
//     return data;
//   }
// }

// class CourseResonse {
//   int? status;
//   CourseResult? result;
//
//   CourseResonse({this.status, this.result});
//
//   CourseResonse.fromJson(Map<String, dynamic> json) {
//     status = json['status'];
//     result =
//     json['result'] != null ? new CourseResult.fromJson(json['result']) : null;
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['status'] = this.status;
//     if (this.result != null) {
//       data['result'] = this.result!.toJson();
//     }
//     return data;
//   }
// }

// class CourseResult {
//   List<String>? purchased;
//   List<CourseOffered>? offered;
//   List<CourseTestSeries>? testSeries;
//   List<CourseVersions>? versions;
//
//   CourseResult({this.purchased, this.offered, this.testSeries, this.versions});
//
//   CourseResult.fromJson(Map<String, dynamic> json) {
//     // if (json['purchased'] != null) {
//     //   purchased = <Null>[];
//     //   json['purchased'].forEach((v) {
//     //     purchased!.add(Null.fromJson(v));
//     //   });
//     // }
//     if (json['offered'] != null) {
//       offered = <CourseOffered>[];
//       json['offered'].forEach((v) {
//         offered!.add(new CourseOffered.fromJson(v));
//       });
//     }
//     if (json['testSeries'] != null) {
//       testSeries = <CourseTestSeries>[];
//       json['testSeries'].forEach((v) {
//         testSeries!.add(new CourseTestSeries.fromJson(v));
//       });
//     }
//     if (json['versions'] != null) {
//       versions = <CourseVersions>[];
//       json['versions'].forEach((v) {
//         versions!.add(new CourseVersions.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     // if (this.purchased != null) {
//     //   data['purchased'] = this.purchased!.map((v) => v.toJson()).toList();
//     // }
//     if (this.offered != null) {
//       data['offered'] = this.offered!.map((v) => v.toJson()).toList();
//     }
//     if (this.testSeries != null) {
//       data['testSeries'] = this.testSeries!.map((v) => v.toJson()).toList();
//     }
//     if (this.versions != null) {
//       data['versions'] = this.versions!.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }

// class CourseOffered {
//   String? bookName;
//   String? bookImage;
//   String? bookDes;
//   String? price;
//   String? offerPercent;
//   String? courseId;
//
//   CourseOffered(
//       {this.bookName,
//         this.bookImage,
//         this.bookDes,
//         this.price,
//         this.offerPercent,
//         this.courseId});
//
//   CourseOffered.fromJson(Map<String, dynamic> json) {
//     bookName = json['BookName'];
//     bookImage = json['BookImage'];
//     bookDes = json['Book_Des'];
//     price = json['price'];
//     offerPercent = json['offer_percent'];
//     courseId = json['course_id'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['BookName'] = this.bookName;
//     data['BookImage'] = this.bookImage;
//     data['Book_Des'] = this.bookDes;
//     data['price'] = this.price;
//     data['offer_percent'] = this.offerPercent;
//     data['course_id'] = this.courseId;
//     return data;
//   }
// }

// class CourseTestSeries {
//   String? testType;
//   String? testid;
//   String? price;
//   String? offer;
//   String? image;
//   String? description;
//   String? isDisplay;
//   String? purchasedId;
//
//   CourseTestSeries(
//       {this.testType,
//         this.testid,
//         this.price,
//         this.offer,
//         this.image,
//         this.description,
//         this.isDisplay,
//         this.purchasedId});
//
//   CourseTestSeries.fromJson(Map<String, dynamic> json) {
//     testType = json['test_type'];
//     testid = json['testid'];
//     price = json['price'];
//     offer = json['offer'];
//     image = json['image'];
//     description = json['description'];
//     isDisplay = json['is_display'];
//     purchasedId = json['purchased_id'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['test_type'] = this.testType;
//     data['testid'] = this.testid;
//     data['price'] = this.price;
//     data['offer'] = this.offer;
//     data['image'] = this.image;
//     data['description'] = this.description;
//     data['is_display'] = this.isDisplay;
//     data['purchased_id'] = this.purchasedId;
//     return data;
//   }
// }


// class CourseVersions {
//   String? appName;
//   String? appVersion;
//
//   CourseVersions({this.appName, this.appVersion});
//
//   CourseVersions.fromJson(Map<String, dynamic> json) {
//     appName = json['app_name'];
//     appVersion = json['app_version'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['app_name'] = this.appName;
//     data['app_version'] = this.appVersion;
//     return data;
//   }
// }




class CourseModels {
  CourseResonse? resonse;

  CourseModels({this.resonse});

  CourseModels.fromJson(Map<String, dynamic> json) {
    resonse =
    json['resonse'] != null ? new CourseResonse.fromJson(json['resonse']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.resonse != null) {
      data['resonse'] = this.resonse!.toJson();
    }
    return data;
  }
}

class CourseResonse {
  int? status;
  CourseResult? result;

  CourseResonse({this.status, this.result});

  CourseResonse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    result =
    json['result'] != null ? new CourseResult.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.result != null) {
      data['result'] = this.result!.toJson();
    }
    return data;
  }
}

class CourseResult {
  List<CourseOffered>? offered;
  List<CourseTestSeries>? testSeries;
  List<CourseVersions>? versions;

  CourseResult({this.offered, this.testSeries, this.versions});

  CourseResult.fromJson(Map<String, dynamic> json) {
    if (json['offered'] != null) {
      offered = <CourseOffered>[];
      json['offered'].forEach((v) {
        offered!.add(new CourseOffered.fromJson(v));
      });
    }
    if (json['testSeries'] != null) {
      testSeries = <CourseTestSeries>[];
      json['testSeries'].forEach((v) {
        testSeries!.add(new CourseTestSeries.fromJson(v));
      });
    }
    if (json['versions'] != null) {
      versions = <CourseVersions>[];
      json['versions'].forEach((v) {
        versions!.add(new CourseVersions.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.offered != null) {
      data['offered'] = this.offered!.map((v) => v.toJson()).toList();
    }
    if (this.testSeries != null) {
      data['testSeries'] = this.testSeries!.map((v) => v.toJson()).toList();
    }
    if (this.versions != null) {
      data['versions'] = this.versions!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CourseOffered {
  String? bookName;
  String? bookImage;
  String? bookDes;
  String? price;
  String? offerPercent;
  String? courseId;

  CourseOffered(
      {this.bookName,
        this.bookImage,
        this.bookDes,
        this.price,
        this.offerPercent,
        this.courseId});

  CourseOffered.fromJson(Map<String, dynamic> json) {
    bookName = json['BookName'];
    bookImage = json['BookImage'];
    bookDes = json['Book_Des'];
    price = json['price'];
    offerPercent = json['offer_percent'];
    courseId = json['course_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['BookName'] = this.bookName;
    data['BookImage'] = this.bookImage;
    data['Book_Des'] = this.bookDes;
    data['price'] = this.price;
    data['offer_percent'] = this.offerPercent;
    data['course_id'] = this.courseId;
    return data;
  }
}

class CourseTestSeries {
  String? testType;
  String? testid;
  String? price;
  String? offer;
  String? image;
  String? description;
  String? isDisplay;
  String? purchasedId;

  CourseTestSeries(
      {this.testType,
        this.testid,
        this.price,
        this.offer,
        this.image,
        this.description,
        this.isDisplay,
        this.purchasedId});

  CourseTestSeries.fromJson(Map<String, dynamic> json) {
    testType = json['test_type'];
    testid = json['testid'];
    price = json['price'];
    offer = json['offer'];
    image = json['image'];
    description = json['description'];
    isDisplay = json['is_display'];
    purchasedId = json['purchased_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['test_type'] = this.testType;
    data['testid'] = this.testid;
    data['price'] = this.price;
    data['offer'] = this.offer;
    data['image'] = this.image;
    data['description'] = this.description;
    data['is_display'] = this.isDisplay;
    data['purchased_id'] = this.purchasedId;
    return data;
  }
}

class CourseVersions {
  String? appName;
  String? appVersion;

  CourseVersions({this.appName, this.appVersion});

  CourseVersions.fromJson(Map<String, dynamic> json) {
    appName = json['app_name'];
    appVersion = json['app_version'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['app_name'] = this.appName;
    data['app_version'] = this.appVersion;
    return data;
  }
}
