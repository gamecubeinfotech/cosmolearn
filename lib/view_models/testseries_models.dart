class TestSeriesModels {
  TestSeriesResonse? resonse;

  TestSeriesModels({this.resonse});

  TestSeriesModels.fromJson(Map<String, dynamic> json) {
    resonse = json['resonse'] != null
        ? new TestSeriesResonse.fromJson(json['resonse'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.resonse != null) {
      data['resonse'] = this.resonse!.toJson();
    }
    return data;
  }
}

class TestSeriesResonse {
  int? status;
  List<TestSeriesResult>? result;

  TestSeriesResonse({this.status, this.result});

  TestSeriesResonse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['result'] != null) {
      result = <TestSeriesResult>[];
      json['result'].forEach((v) {
        result!.add(new TestSeriesResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.result != null) {
      data['result'] = this.result!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TestSeriesResult {
  String? testType;
  String? testid;
  String? price;
  String? offer;
  String? image;
  String? description;
  String? isDisplay;
  String? purchasedId;

  TestSeriesResult(
      {this.testType,
      this.testid,
      this.price,
      this.offer,
      this.image,
      this.description,
      this.isDisplay,
      this.purchasedId});

  TestSeriesResult.fromJson(Map<String, dynamic> json) {
    testType = json['test_type'];
    testid = json['testid'];
    price = json['price'];
    offer = json['offer'];
    image = json['image'];
    description = json['description'];
    isDisplay = json['is_display'];
    purchasedId = json['purchased_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['test_type'] = this.testType;
    data['testid'] = this.testid;
    data['price'] = this.price;
    data['offer'] = this.offer;
    data['image'] = this.image;
    data['description'] = this.description;
    data['is_display'] = this.isDisplay;
    data['purchased_id'] = this.purchasedId;
    return data;
  }
}
