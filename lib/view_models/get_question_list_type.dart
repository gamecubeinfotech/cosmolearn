class GetQuestionsListByType {
  GetQuestionsListResonse? resonse;

  GetQuestionsListByType({this.resonse});

  GetQuestionsListByType.fromJson(Map<String, dynamic> json) {
    resonse = json['resonse'] != null
        ? new GetQuestionsListResonse.fromJson(json['resonse'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.resonse != null) {
      data['resonse'] = this.resonse!.toJson();
    }
    return data;
  }
}

class GetQuestionsListResonse {
  int? status;
  List<GetQuestionsListResult>? result;

  GetQuestionsListResonse({this.status, this.result});

  GetQuestionsListResonse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['result'] != null) {
      result = <GetQuestionsListResult>[];
      json['result'].forEach((v) {
        result!.add(new GetQuestionsListResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.result != null) {
      data['result'] = this.result!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class GetQuestionsListResult {
  String? id;
  String? clientId;
  String? ttnameId;
  String? bookId;
  String? chapterId;
  String? userMobile;
  String? questionNumber;
  String? answerMarked;
  String? correctAnswer;
  String? optionD;
  String? checkStatus;
  String? timeTakenToSolve;
  String? diffIndex;
  String? isAttempted;
  String? created;
  String? modified;
  String? qNo;
  String? question;
  String? optionA;
  String? optionB;
  String? optionC;
  String? explaination;
  String? imageExplain;
  String? questionImage;
  String? optionAImage;
  String? optionBImage;
  String? optionCImage;
  String? optionDImage;

  GetQuestionsListResult(
      {this.id,
      this.clientId,
      this.ttnameId,
      this.bookId,
      this.chapterId,
      this.userMobile,
      this.questionNumber,
      this.answerMarked,
      this.correctAnswer,
      this.optionD,
      this.checkStatus,
      this.timeTakenToSolve,
      this.diffIndex,
      this.isAttempted,
      this.created,
      this.modified,
      this.qNo,
      this.question,
      this.optionA,
      this.optionB,
      this.optionC,
      this.explaination,
      this.imageExplain,
      this.questionImage,
      this.optionAImage,
      this.optionBImage,
      this.optionCImage,
      this.optionDImage});

  GetQuestionsListResult.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    clientId = json['Client_id'];
    ttnameId = json['ttname_id'];
    bookId = json['book_id'];
    chapterId = json['chapter_id'];
    userMobile = json['user_mobile'];
    questionNumber = json['question_number'];
    answerMarked = json['answer_marked'];
    correctAnswer = json['correct_answer'];
    optionD = json['option_d'];
    checkStatus = json['check_status'];
    timeTakenToSolve = json['time_taken_to_solve'];
    diffIndex = json['diff_index'];
    isAttempted = json['is_attempted'];
    created = json['created'];
    modified = json['modified'];
    qNo = json['q_no'];
    question = json['question'];
    optionA = json['option_a'];
    optionB = json['option_b'];
    optionC = json['option_c'];
    explaination = json['explaination'];
    imageExplain = json['image_explain'];
    questionImage = json['question_image'];
    optionAImage = json['option_a_image'];
    optionBImage = json['option_b_image'];
    optionCImage = json['option_c_image'];
    optionDImage = json['option_d_image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['Client_id'] = this.clientId;
    data['ttname_id'] = this.ttnameId;
    data['book_id'] = this.bookId;
    data['chapter_id'] = this.chapterId;
    data['user_mobile'] = this.userMobile;
    data['question_number'] = this.questionNumber;
    data['answer_marked'] = this.answerMarked;
    data['correct_answer'] = this.correctAnswer;
    data['option_d'] = this.optionD;
    data['check_status'] = this.checkStatus;
    data['time_taken_to_solve'] = this.timeTakenToSolve;
    data['diff_index'] = this.diffIndex;
    data['is_attempted'] = this.isAttempted;
    data['created'] = this.created;
    data['modified'] = this.modified;
    data['q_no'] = this.qNo;
    data['question'] = this.question;
    data['option_a'] = this.optionA;
    data['option_b'] = this.optionB;
    data['option_c'] = this.optionC;
    data['explaination'] = this.explaination;
    data['image_explain'] = this.imageExplain;
    data['question_image'] = this.questionImage;
    data['option_a_image'] = this.optionAImage;
    data['option_b_image'] = this.optionBImage;
    data['option_c_image'] = this.optionCImage;
    data['option_d_image'] = this.optionDImage;
    return data;
  }
}
