class TestSeriesNameModels {
  TestSeriesNameResonse? resonse;

  TestSeriesNameModels({this.resonse});

  TestSeriesNameModels.fromJson(Map<String, dynamic> json) {
    resonse =
    json['resonse'] != null ? new TestSeriesNameResonse.fromJson(json['resonse']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.resonse != null) {
      data['resonse'] = this.resonse!.toJson();
    }
    return data;
  }
}

class TestSeriesNameResonse {
  int? status;
  List<TestSeriesNameResult>? result;

  TestSeriesNameResonse({this.status, this.result});

  TestSeriesNameResonse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['result'] != null) {
      result = <TestSeriesNameResult>[];
      json['result'].forEach((v) {
        result!.add(TestSeriesNameResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['status'] = this.status;
    if (this.result != null) {
      data['result'] = this.result!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TestSeriesNameResult {
  String? testTypeId;
  String? ttnameId;
  String? testId;
  String? testType;
  String? testTypeName;
  String? timeForTheTest;
  String? noOfMcqs;
  String? marksForQuestion;
  String? negativeMarking;
  String? negativeMarks;
  String? lockStatus;
  String? isAttemptedTest;

  TestSeriesNameResult(
      {this.testTypeId,
        this.ttnameId,
        this.testId,
        this.testType,
        this.testTypeName,
        this.timeForTheTest,
        this.noOfMcqs,
        this.marksForQuestion,
        this.negativeMarking,
        this.negativeMarks,
        this.lockStatus,
        this.isAttemptedTest});

  TestSeriesNameResult.fromJson(Map<String, dynamic> json) {
    testTypeId = json['test_type_id'];
    ttnameId = json['ttname_id'];
    testId = json['test_id'];
    testType = json['test_type'];
    testTypeName = json['test_type_name'];
    timeForTheTest = json['time_for_the_test'];
    noOfMcqs = json['no_of_mcqs'];
    marksForQuestion = json['marks_for_question'];
    negativeMarking = json['negative_marking'];
    negativeMarks = json['negative_marks'];
    lockStatus = json['Lock_status'];
    isAttemptedTest = json['isAttemptedTest'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['test_type_id'] = this.testTypeId;
    data['ttname_id'] = this.ttnameId;
    data['test_id'] = this.testId;
    data['test_type'] = this.testType;
    data['test_type_name'] = this.testTypeName;
    data['time_for_the_test'] = this.timeForTheTest;
    data['no_of_mcqs'] = this.noOfMcqs;
    data['marks_for_question'] = this.marksForQuestion;
    data['negative_marking'] = this.negativeMarking;
    data['negative_marks'] = this.negativeMarks;
    data['Lock_status'] = this.lockStatus;
    data['isAttemptedTest'] = this.isAttemptedTest;
    return data;
  }
}
