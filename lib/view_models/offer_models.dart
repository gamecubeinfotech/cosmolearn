class OfferModels {
  OfferResonse? resonse;

  OfferModels({this.resonse});

  OfferModels.fromJson(Map<String, dynamic> json) {
    resonse =
    json['resonse'] != null ? new OfferResonse.fromJson(json['resonse']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.resonse != null) {
      data['resonse'] = this.resonse!.toJson();
    }
    return data;
  }
}

class OfferResonse {
  int? status;
  List<OfferResult>? result;

  OfferResonse({this.status, this.result});

  OfferResonse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['result'] != null) {
      result = <OfferResult>[];
      json['result'].forEach((v) {
        result!.add(new OfferResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.result != null) {
      data['result'] = this.result!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class OfferResult {
  String? id;
  String? offerTitle;
  String? offerDescription;
  String? offerPrice;

  OfferResult({this.id, this.offerTitle, this.offerDescription, this.offerPrice});

  OfferResult.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    offerTitle = json['offer_title'];
    offerDescription = json['offer_description'];
    offerPrice = json['offer_price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['offer_title'] = this.offerTitle;
    data['offer_description'] = this.offerDescription;
    data['offer_price'] = this.offerPrice;
    return data;
  }
}
