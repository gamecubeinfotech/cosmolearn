class SubmitResultModels {
  SubmitResultResonse? resonse;

  SubmitResultModels({this.resonse});

  SubmitResultModels.fromJson(Map<String, dynamic> json) {
    resonse =
    json['resonse'] != null ? new SubmitResultResonse.fromJson(json['resonse']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.resonse != null) {
      data['resonse'] = this.resonse!.toJson();
    }
    return data;
  }
}

class SubmitResultResonse {
  int? status;
  SubmitResultResult? result;

  SubmitResultResonse({this.status, this.result});

  SubmitResultResonse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    result =
    json['result'] != null ? new SubmitResultResult.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.result != null) {
      data['result'] = this.result!.toJson();
    }
    return data;
  }
}

class SubmitResultResult {
  String? testName;
  String? markes;
  String? percentage;
  String? attemptedCount;
  String? negMarkQCount;
  String? totalCount;
  String? marksOfQuestion;
  String? negativeMarks;
  String? totalMarks;
  String? skipQuestionCount;
  String? correctQuestionCount;
  String? created;
  String? modified;

  SubmitResultResult(
      {this.testName,
        this.markes,
        this.percentage,
        this.attemptedCount,
        this.negMarkQCount,
        this.totalCount,
        this.marksOfQuestion,
        this.negativeMarks,
        this.totalMarks,
        this.skipQuestionCount,
        this.correctQuestionCount,
        this.created,
        this.modified});

  SubmitResultResult.fromJson(Map<String, dynamic> json) {
    testName = json['test_name'];
    markes = json['markes'];
    percentage = json['percentage'];
    attemptedCount = json['attempted_count'];
    negMarkQCount = json['neg_mark_q_count'];
    totalCount = json['total_count'];
    marksOfQuestion = json['marks_of_question'];
    negativeMarks = json['negative_marks'];
    totalMarks = json['total_marks'];
    skipQuestionCount = json['skip_question_count'];
    correctQuestionCount = json['correct_question_count'];
    created = json['created'];
    modified = json['modified'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['test_name'] = this.testName;
    data['markes'] = this.markes;
    data['percentage'] = this.percentage;
    data['attempted_count'] = this.attemptedCount;
    data['neg_mark_q_count'] = this.negMarkQCount;
    data['total_count'] = this.totalCount;
    data['marks_of_question'] = this.marksOfQuestion;
    data['negative_marks'] = this.negativeMarks;
    data['total_marks'] = this.totalMarks;
    data['skip_question_count'] = this.skipQuestionCount;
    data['correct_question_count'] = this.correctQuestionCount;
    data['created'] = this.created;
    data['modified'] = this.modified;
    return data;
  }
}
