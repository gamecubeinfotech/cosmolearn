import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import '../appUtilities/AppPrefrences.dart';
import '../appUtilities/app_circular_loader.dart';
import '../appUtilities/app_colors.dart';
import '../appUtilities/app_constants.dart';
import '../appUtilities/routes/routes_name.dart';
import '../appUtilities/utils.dart';
import 'otp_screen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override

  void initState() {
    // TODO: implement initState
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(statusBarColor: mainColor,

        statusBarIconBrightness: Brightness.dark,
        /* set Status bar icons color in Android devices.*/
        statusBarBrightness: Brightness.light)
    );
  }

  TextEditingController mobileOTP = TextEditingController();



  @override
  Widget build(BuildContext context) {

    return SafeArea(
        child: Scaffold(
         backgroundColor: mainColor,
        body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: mainColor,
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 40),
                child: Image.asset(
                  'assets/images/logo.png',
                  fit: BoxFit.fill,
                  height: 70,
                ),
              ),
              Text(
                AppConstants.welcome,
                style: GoogleFonts.poppins(
                  fontSize: 25,
                  fontWeight: FontWeight.w600,
                  height: 1.25,
                  color: blackColor,
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                AppConstants.loginPageText,
                style: GoogleFonts.poppins(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  height: 1.6,
                  color: subText,
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Container(
                height: 50,
                margin: const EdgeInsets.only(top: 10, bottom: 20),
                padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 9),
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(5),
                    ),
                    color: Colors.white),
                child: TextField(
                  controller: mobileOTP,
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    FilteringTextInputFormatter.digitsOnly // FilteringTextInputFormatter.digitsOnly
                  ],
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: AppConstants.enterMobile,
                    hintStyle: GoogleFonts.poppins(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      height: 1.25,
                      color: textField,
                    ),
                  ),
                ),
              ),
              Text(
                AppConstants.otpSent,
                style: GoogleFonts.poppins(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  height: 1.25,
                  color: lightRed,
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Material(
                color: lightBlue,
                borderRadius: BorderRadius.circular(50),
                child: InkWell(
                  borderRadius: BorderRadius.circular(50),
                  onTap: () {
                    if(mobileOTP.text.toString().isEmpty){
                      Fluttertoast.showToast(msg: 'Please Enter Mobile Number');
                    }else if (mobileOTP.text.toString().isEmpty) {
                      Fluttertoast.showToast(msg: 'Please Enter Valid Mobile Number');
                    }else {
                      otpData().then((response){
                        var data=jsonDecode(response)["resonse"];
                        print("Status code: ${data['status']}");
                        if(data['status']==200) {
                          AppPrefrence.putString("mobile",mobileOTP.text.toString());

                          Navigator.push(context, MaterialPageRoute(builder: (context) => otpScreen(mobileOTP.text.toString()),));
                        }
                        // Fluttertoast.showToast(msg: data['result']);
                      });
                    }
                  },
                  child: Container(
                    height: 50,
                    width: MediaQuery.of(context).size.width,
                    alignment: Alignment.center,
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(50)),
                    child: Text(
                      AppConstants.login,
                      style: GoogleFonts.poppins(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        height: 1.25,
                        color: whiteColor,
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Material(
                color: lightRed,
                borderRadius: BorderRadius.circular(50),
                child: InkWell(
                  borderRadius: BorderRadius.circular(50),
                  onTap: () {
                    Navigator.pushNamed(context, RoutesName.signUp);
                  },
                  child: Container(
                    height: 50,
                    width: MediaQuery.of(context).size.width,
                    alignment: Alignment.center,
                    child: Text(
                      AppConstants.sign_Up,
                      style: GoogleFonts.poppins(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        height: 1.25,
                        color: whiteColor,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    ));
  }
  Future<dynamic> otpData() async {
    AppLoaderProgress.showLoader(context);
    final response = await http.post(Uri.parse(UtilsMethod.baseUrl),
      body: {
        "api_name": "sendOtp",
        "mobile":mobileOTP.text.toString(),
      },
    );
    AppLoaderProgress.hideLoader(context);
    return response.body;
  }
}
