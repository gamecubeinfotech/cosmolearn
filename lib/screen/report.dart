import 'dart:convert';

import 'package:cosmolearn/appUtilities/routes/routes_name.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import '../appUtilities/AppPrefrences.dart';
import '../appUtilities/app_circular_loader.dart';
import '../appUtilities/app_colors.dart';
import '../appUtilities/app_constants.dart';
import '../appUtilities/utils.dart';
import '../view_models/report_models.dart';
import 'all_question_list.dart';

class Report extends StatefulWidget {
  const Report({super.key});

  @override
  State<Report> createState() => _ReportState();
}

class _ReportState extends State<Report> {
  ReportModels reportModels = ReportModels();

  String formatStringAsPercentage(String input, int decimalPlaces) {
    final trimmedInput = input.replaceAll('%', '');
    final number = double.tryParse(trimmedInput);
    if (number != null) {
      final formattedNumber = number.toStringAsFixed(decimalPlaces);
      return '$formattedNumber%';
    } else {
      return input;
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      getReportList();
    });
  }


  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: mainColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,

          iconTheme: const IconThemeData(
            color: Colors.black, //change your color here
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
          title: Padding(
            padding: const EdgeInsets.only(left: 8),
            child: Text(
              'Report',
              style: GoogleFonts.poppins(fontSize: 18,fontWeight: FontWeight.w600,color: blackColor),
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [

              Container(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: ListView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: allgetReports.length,
                  scrollDirection: Axis.vertical,
                  itemBuilder: (context, index) {
                    ReportResult report = allgetReports[index];
                    final formattedString = formatStringAsPercentage(report.percentage.toString(), 2);
                    return Container(
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          color: whiteColor,
                          borderRadius: BorderRadius.circular(10)),
                      margin: const EdgeInsets.all(10),
                      child: Column(
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Row(
                                children: [
                                  Text(
                                    report.testName.toString(),
                                    style: GoogleFonts.poppins(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600,
                                      height: 1.4,
                                      color: blackColor,
                                    ),
                                  ),
                                  const Spacer(),
                                  Text(
                                    formattedString,
                                    textAlign: TextAlign.right,
                                    style: GoogleFonts.poppins(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600,
                                      height: 1.4,
                                      color: lightBlue,
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.play_circle_outline,
                                        size: 18,
                                        color: subText,
                                      ),
                                      const SizedBox(
                                        width: 5,
                                      ),
                                      Text(
                                        report.totalCount.toString(),
                                        style: GoogleFonts.poppins(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500,
                                          height: 1.25,
                                          color: textField,
                                        ),
                                      ),
                                      const SizedBox(
                                        width: 5,
                                      ),
                                      Text(
                                        AppConstants.questions,
                                        style: GoogleFonts.poppins(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500,
                                          height: 1.25,
                                          color: textField,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  // Row(
                                  //   children: [
                                  //     Icon(
                                  //       Icons.watch_later_outlined,
                                  //       size: 18,
                                  //       color: subText,
                                  //     ),
                                  //     SizedBox(
                                  //       width: 5,
                                  //     ),
                                  //     Text(
                                  //       '30 min',
                                  //       style: GoogleFonts.poppins(
                                  //         fontSize: 12,
                                  //         fontWeight: FontWeight.w500,
                                  //         height: 1.25,
                                  //         color: textField,
                                  //       ),
                                  //     ),
                                  //   ],
                                  // ),
                                  const Spacer(),
                                  Text(
                                    AppConstants.score,
                                    textAlign: TextAlign.right,
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500,
                                      height: 1.25,
                                      color: textField,
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  Text(
                                    AppConstants.attempted,
                                    style: GoogleFonts.poppins(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600,
                                      height: 1.4,
                                      color: blackColor,
                                    ),
                                  ),
                                  Text(
                                    report.attemptedCount.toString(),
                                    style: GoogleFonts.poppins(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600,
                                      height: 1.4,
                                      color: textField,
                                    ),
                                  ),
                                  const Spacer(),
                                  Text(
                                    AppConstants.marks,
                                    style: GoogleFonts.poppins(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600,
                                      height: 1.4,
                                      color: blackColor,
                                    ),
                                  ),
                                  Text(
                                    report.markes.toString(),
                                    style: GoogleFonts.poppins(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600,
                                      height: 1.4,
                                      color: textField,
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                      flex: 1,
                                      child: Column(
                                        children: [
                                          Container(
                                            height: 20,
                                            color: brightRed,
                                          ),
                                          const SizedBox(height: 3,),
                                          Row(mainAxisAlignment: MainAxisAlignment.center,children: [
                                            Text(
                                            AppConstants.correct + ': ',
                                            style: GoogleFonts.poppins(
                                              fontSize: 12,
                                              fontWeight: FontWeight.w400,
                                              height: 1.4,
                                              color: blackColor,
                                            ),
                                          ),
                                            Text(
                                              report.correctQuestionCount.toString(),
                                              style: GoogleFonts.poppins(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w400,
                                                height: 1.4,
                                                color: blackColor,
                                              ),
                                            ),
                                          ],)

                                        ],
                                      )),
                                  const SizedBox(
                                    width: 3,
                                  ),
                                  Expanded(
                                      flex: 1,
                                      child: Column(
                                        children: [
                                          Container(
                                            height: 20,
                                            color: lightRed,
                                          ),
                                          const SizedBox(height: 3,),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                            Text(
                                              '${AppConstants.wrong}: ',
                                              style: GoogleFonts.poppins(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w400,
                                                height: 1.4,
                                                color: blackColor,
                                              ),
                                            ),
                                            Text(
                                              report.negMarkQCount.toString(),
                                              style: GoogleFonts.poppins(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w400,
                                                height: 1.4,
                                                color: blackColor,
                                              ),
                                            ),
                                          ],)
                                        ],
                                      )),
                                  const SizedBox(
                                    width: 3,
                                  ),
                                  Expanded(
                                      flex: 1,
                                      child: Column(
                                        children: [
                                          Container(
                                            height: 20,
                                            color: lightBlue,
                                          ),
                                          const SizedBox(height: 3,),
                                          Row(mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                            Text(
                                              '${AppConstants.skipped}: ',
                                              style: GoogleFonts.poppins(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w400,
                                                height: 1.4,
                                                color: blackColor,
                                              ),
                                            ),
                                            Text(
                                              report.skipQuestionCount.toString(),
                                              style: GoogleFonts.poppins(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w400,
                                                height: 1.4,
                                                color: blackColor,
                                              ),
                                            ),
                                          ],)
                                        ],
                                      )),
                                ],
                              )
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Divider(
                            height: 2,
                            thickness: 1,
                            color: borderColor,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: InkWell(
                                  onTap: (){
                                    // Navigator.pushNamed(context, RoutesName.allQuestionList, arguments: {'id': report.id.toString(), 'type': "CORRECT",},);
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => AllQuestionList(report.id.toString(), "CORRECT",),));
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.symmetric(horizontal: 5),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 5, horizontal: 10),
                                    decoration: BoxDecoration(
                                      color: lightSky,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: Text(
                                      AppConstants.correct,
                                      style: GoogleFonts.poppins(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500,
                                        height: 1.4,
                                        color: lightBlue,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: InkWell(
                                  onTap: (){
                                    // Navigator.pushNamed(context, RoutesName.allQuestionList, arguments: {
                                    //   'id': report.id.toString(),
                                    //   'type': "WRONG",
                                    // },);
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => AllQuestionList(report.id.toString(), "WRONG",),));
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.symmetric(horizontal: 5),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 5, horizontal: 10),
                                    decoration: BoxDecoration(
                                      color: lightSky,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: Text(
                                      AppConstants.wrong,
                                      style: GoogleFonts.poppins(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500,
                                        height: 1.4,
                                        color: lightBlue,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: InkWell(
                                  onTap: (){
                                    // Navigator.pushNamed(context, RoutesName.allQuestionList, arguments: {
                                    //   'id': report.id.toString(),
                                    //   'type': "SKIPPED",
                                    // },);
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => AllQuestionList(report.id.toString(), "SKIPPED",),));
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.symmetric(horizontal: 5),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 5, horizontal: 10),
                                    decoration: BoxDecoration(
                                      color: lightSky,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: Text(
                                      AppConstants.skipped,
                                      style: GoogleFonts.poppins(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500,
                                        height: 1.4,
                                        color: lightBlue,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: InkWell(
                                  onTap: (){
                                    // Navigator.pushNamed(context, RoutesName.allQuestionList, arguments: {
                                    //   'id': report.id.toString(),
                                    //   'type': "ALL",
                                    // },);
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => AllQuestionList(report.id.toString(), "ALL",),));
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.symmetric(horizontal: 5),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 5, horizontal: 10),
                                    decoration: BoxDecoration(
                                      color: lightSky,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: Text(
                                      AppConstants.all,
                                      style: GoogleFonts.poppins(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500,
                                        height: 1.4,
                                        color: lightBlue,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<ReportResult> allgetReports = [];

  Future<void> getReportList() async {
    AppLoaderProgress.showLoader(context);
    String clientId = await AppPrefrence.getString("id");
    final response = await http.post(Uri.parse(UtilsMethod.baseUrl),
      body: {
        "api_name": "getTestResultList",
        "client_id": clientId,
      },
    );
    if (response.statusCode == 200) {
      var Response = jsonDecode(response.body);
      print("allgetReports ${jsonEncode(Response)}");
      allgetReports = ReportModels.fromJson(Response).resonse!.result!;
      print("allgetReports ${jsonDecode(jsonEncode(allgetReports))}");
      setState(() {
      });
    }

    AppLoaderProgress.hideLoader(context);
  }

}
