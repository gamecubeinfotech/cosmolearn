import 'dart:convert';

import 'package:cosmolearn/appUtilities/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import '../appUtilities/app_circular_loader.dart';
import '../appUtilities/app_constants.dart';
import '../appUtilities/utils.dart';
import '../view_models/get_question_list_type.dart';
import '../view_models/question_list_models.dart';

class AllQuestionList extends StatefulWidget {
  String reportID;
  String type;
   AllQuestionList(this.reportID, this.type,);

  @override
  State<AllQuestionList> createState() => _AllQuestionListState();
}

class _AllQuestionListState extends State<AllQuestionList> {

  // Object? indexToAlphabet(int index) {
  //   if (index >= 0 && index < 26) {
  //     return String.fromCharCode(65 + index);
  //   } else if (index >= 26) {
  //     return (index - 25).toString();
  //   }
  //   return null; // Return null for negative indices
  // }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      getQuestionList();
    });
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: mainColor,
      appBar: AppBar(
        iconTheme: const IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text(
          'Question List',
          style: GoogleFonts.poppins(
              fontSize: 16, fontWeight: FontWeight.w600, color: blackColor),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: questionList.length,
                scrollDirection: Axis.vertical,
                itemBuilder: (context, index) {
                  // Object? alphabet = indexToAlphabet(index);

                  GetQuestionsListResult question = questionList[index];
                  return Container(
                    decoration: BoxDecoration(
                        color: whiteColor,
                        borderRadius: BorderRadius.circular(10)),
                    margin: const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          children: [
                            Container(
                              padding: EdgeInsets.all(10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Text(
                                          question.question.toString(),
                                          style: GoogleFonts.poppins(
                                            fontSize: 14,
                                            height: 1.25,

                                            fontWeight: FontWeight.w600,
                                            color: blackColor,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: 10,),
                                  Image.network(question.questionImage.toString(),fit: BoxFit.fill,
                                    width: MediaQuery.of(context).size.width,
                                    height: 110,
                                    errorBuilder: (context, error, stackTrace) {
                                      return Container();
                                    },
                                  ),
                                  const SizedBox(height: 10,),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          'A. ${question.optionA}',
                                          textAlign: TextAlign.start,
                                          style: GoogleFonts.poppins(
                                            fontSize: 14,
                                            height: 1,
                                            fontWeight: FontWeight.w500,
                                            color: textField,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          'B. ${question.optionB}',
                                          textAlign: TextAlign.start,
                                          style: GoogleFonts.poppins(
                                            fontSize: 14,
                                            height: 1,
                                            fontWeight: FontWeight.w500,
                                            color: textField,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ), 
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          'C. ${question.optionC}',
                                          textAlign: TextAlign.start,
                                          style: GoogleFonts.poppins(
                                            fontSize: 14,
                                            height: 1,
                                            fontWeight: FontWeight.w500,
                                            color: textField,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ), 
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          'D. ${question.optionD}',
                                          textAlign: TextAlign.start,
                                          style: GoogleFonts.poppins(
                                            fontSize: 14,
                                            height: 1,
                                            fontWeight: FontWeight.w500,
                                            color: textField,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ), 
                                ],
                              ),
                            ),
                            // ListView.builder(
                            //   physics: NeverScrollableScrollPhysics(),
                            //   shrinkWrap: true,
                            //   itemCount: 4,
                            //   scrollDirection: Axis.vertical,
                            //   itemBuilder: (context, index) {
                            //     int Indexlist = index + 1;
                            //     return Padding(
                            //       padding: EdgeInsets.symmetric(vertical: 3,horizontal: 10),
                            //       child: Row(
                            //         children: [
                            //           Text(
                            //             '$Indexlist.',
                            //             style: GoogleFonts.poppins(
                            //               fontSize: 14,
                            //               fontWeight: FontWeight.w500,
                            //               color: textField,
                            //             ),
                            //           ),
                            //           SizedBox(
                            //             width: 5,
                            //           ),
                            //           Text(
                            //             'Module 1. Mock test 101',
                            //             style: GoogleFonts.poppins(
                            //               fontSize: 14,
                            //               fontWeight: FontWeight.w500,
                            //               color: textField,
                            //             ),
                            //           ),
                            //         ],
                            //       ),
                            //     );
                            //   },
                            // ),
                          ],
                        ),
                        const SizedBox(height: 5,),
                        Container(
                          alignment: Alignment.center,
                          height: 50,
                          width: MediaQuery.of(context).size.width,
                          color: lightSky,
                          padding: EdgeInsets.symmetric(horizontal: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Expanded(
                                flex: 1,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Correct Answer:',
                                      style: GoogleFonts.poppins(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500,
                                        color: blackColor,
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Container(
                                      padding:
                                      const EdgeInsets.symmetric(horizontal: 10, vertical: 3),
                                      decoration: BoxDecoration(
                                          color: lightBlue,
                                          borderRadius: BorderRadius.circular(40)),
                                      child: Text(
                                        question.correctAnswer.toString(),
                                        style: GoogleFonts.poppins(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          color: whiteColor,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Answer Opted:',
                                      style: GoogleFonts.poppins(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500,
                                        color: blackColor,
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Container(
                                      padding:
                                      const EdgeInsets.symmetric(horizontal: 10, vertical: 3),
                                      decoration: BoxDecoration(
                                          color: lightRed,
                                          borderRadius: BorderRadius.circular(40)),
                                      child: Text(
                                        question.answerMarked.toString(),
                                        style: GoogleFonts.poppins(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          color: whiteColor,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          margin: const EdgeInsets.symmetric(horizontal: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const SizedBox(
                                height: 10,
                              ),
                              question.explaination != '' && question.explaination != null ? Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    AppConstants.explanation,
                                    style: GoogleFonts.poppins(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600,
                                      color: lightBlue,
                                    ),
                                  ),
                                  const SizedBox(height: 10,),
                                  Image.network(question.imageExplain.toString(),fit: BoxFit.fill,
                                    width: MediaQuery.of(context).size.width,
                                    height: 110,errorBuilder: (context, error, stackTrace) {
                                      return Container();
                                    },),
                                  const SizedBox(height: 10,),
                                  Text(
                                    question.explaination.toString(),
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500,
                                      height: 1.4,
                                      color: textField,
                                    ),
                                  ),
                                  const SizedBox(height: 10,),
                                ],
                              ) : Container(),
                            ],
                          ),
                        )
                      ],
                    ),
                  );
                },
              ),
            ),

          ],
        ),
      ),
    );
  }
  List<GetQuestionsListResult> questionList = [];
  Future<void> getQuestionList() async {
    AppLoaderProgress.showLoader(context);
    final response = await http.post(Uri.parse(UtilsMethod.baseUrl),
      body: {
        "api_name": "getQuestionsListByType",
        "report_id": widget.reportID,
        "type": widget.type,
      },
    );
    if (response.statusCode == 200) {
      print(widget.reportID);
      print(widget.type);
      var Response = jsonDecode(response.body);
      questionList = GetQuestionsListByType.fromJson(Response).resonse!.result!;
      setState(() {
      });
    }

    AppLoaderProgress.hideLoader(context);
  }

}
