import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pinput/pinput.dart';
import '../appUtilities/AppPrefrences.dart';
import '../appUtilities/app_circular_loader.dart';
import '../appUtilities/app_colors.dart';
import '../appUtilities/app_constants.dart';
import '../appUtilities/routes/routes_name.dart';
import 'package:http/http.dart' as http;
import '../appUtilities/utils.dart';



class otpScreen extends StatefulWidget {
  String otpText;
  otpScreen(this.otpText);

  @override
  State<otpScreen> createState() => _otpScreenState();
}

class _otpScreenState extends State<otpScreen> {
  final focusNode = FocusNode();
  bool showError = false;
  String? otpValue;
  @override
  Widget build(BuildContext context) {
    const length = 4;
    const borderColor = Color.fromRGBO(114, 178, 238, 1);
    const errorColor = Color.fromRGBO(255, 234, 238, 1);
    const fillColor = Color.fromRGBO(222, 231, 240, .57);
    final defaultPinTheme = PinTheme(
      width: 56,
      height: 60,
      textStyle: GoogleFonts.poppins(
        fontSize: 22,
        color: const Color.fromRGBO(30, 60, 87, 1),
      ),
      decoration: BoxDecoration(
        color: fillColor,
        borderRadius: BorderRadius.circular(8),
        border: Border.all(color: Colors.transparent),
      ),
    );

    return SafeArea(
        child: Scaffold(
      backgroundColor: mainColor,
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: mainColor,
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 40),
                child: Image.asset(
                  'assets/images/logo.png',
                  fit: BoxFit.fill,
                  height: 70,
                ),
              ),
              Text(
                AppConstants.verifyOtp,
                style: GoogleFonts.poppins(
                  fontSize: 25,
                  fontWeight: FontWeight.w600,
                  height: 1.25,
                  color: blackColor,
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                'Enter the 4 digit number that we send to ${widget.otpText}',
                style: GoogleFonts.poppins(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  height: 1.6,
                  color: subText,
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              SizedBox(
                height: 68,
                width: MediaQuery.of(context).size.width,
                child: Pinput(
                  length: length,
                  focusNode: focusNode,
                  defaultPinTheme: defaultPinTheme,
                  onCompleted: (pin) {
                    otpValue = pin;
                    setState(() => showError = pin != '5555');
                  },
                  focusedPinTheme: defaultPinTheme.copyWith(
                    height: 68,
                    width: 64,
                    decoration: defaultPinTheme.decoration!.copyWith(
                      border: Border.all(color: borderColor),
                    ),
                  ),
                  errorPinTheme: defaultPinTheme.copyWith(
                    decoration: BoxDecoration(
                      color: errorColor,
                      borderRadius: BorderRadius.circular(8),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Material(
                color: lightBlue,
                borderRadius: BorderRadius.circular(50),
                child: InkWell(
                  borderRadius: BorderRadius.circular(50),
                  onTap: () {
                    if(otpValue!.isEmpty){
                      Fluttertoast.showToast(msg: 'Please Enter OTP');
                    }else {
                      otpData().then((response){
                        var data=jsonDecode(response)["resonse"];
                        print(data['status']);

                        if(data['status']==200){

                          AppPrefrence.putBoolean("isLogin",true);
                          AppPrefrence.putString("id",data['result']['client_id']);
                          AppPrefrence.putString("teacher_id",data['result']['teacher_id']);
                          AppPrefrence.putString("offline",data['result']['offline']);
                          focusNode.unfocus();
                          Navigator.pushNamed(context, RoutesName.home);
                        }
                        Fluttertoast.showToast(msg: data['result']['message']);
                      });
                    }

                  },
                  child: Container(
                    height: 50,
                    width: MediaQuery.of(context).size.width,
                    alignment: Alignment.center,
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(50)),
                    child: Text(
                      AppConstants.verify,
                      style: GoogleFonts.poppins(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        height: 1.25,
                        color: whiteColor,
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
            ],
          ),
        ),
      ),
    ));
  }
  Future<dynamic> otpData() async {
    AppLoaderProgress.showLoader(context);
    final response = await http.post(Uri.parse(UtilsMethod.baseUrl),
      body: {
        "api_name": "verifyOtp",
        "mobile":widget.otpText,
        "otp": otpValue,
        "deviceId":"1213",
        "deviceType":'MOBILE'
      },
    );
    AppLoaderProgress.hideLoader(context);
    return response.body;
  }
}
