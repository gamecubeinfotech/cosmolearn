import 'dart:convert';

import 'package:cosmolearn/appUtilities/routes/routes_name.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import '../appUtilities/app_circular_loader.dart';
import '../appUtilities/app_colors.dart';
import '../appUtilities/utils.dart';
import '../view_models/chapters_models.dart';
import 'course_start.dart';


class CourseDetails extends StatefulWidget {
String bookName;
CourseDetails(this.bookName);

  @override
  State<CourseDetails> createState() => _CourseDetailsState();
}

class _CourseDetailsState extends State<CourseDetails> {

  int tabcontroller = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      getAllChapters();
    });
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: mainColor,
        appBar: AppBar(
          iconTheme: const IconThemeData(
            color: Colors.black, //change your color here
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
          title: Text(
            widget.bookName,
            style: GoogleFonts.poppins(
                fontSize: 16, fontWeight: FontWeight.w600, color: blackColor),
          ),
        ),
        body: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            children: [

              ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: allChaptersList.length,
                scrollDirection: Axis.vertical,
                itemBuilder: (context, index) {
                  ChaptersResult chapter = allChaptersList[index];

                  return Container(
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          color: whiteColor,
                          borderRadius: BorderRadius.circular(10)),
                      margin: const EdgeInsets.all(10),
                      child: Column(
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                flex: 1,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: Image.network(
                                    chapter.chapterImage.toString(),
                                    fit: BoxFit.cover,
                                    height: 80,
                                    errorBuilder: (context, error, stackTrace) {
                                      return Image.asset(
                                        'assets/images/discoverbanner.png',
                                        fit: BoxFit.cover,
                                        height: 80,
                                      );
                                    },
                                  ),

                                ),
                              ),
                              Expanded(
                                flex: 3,
                                child: Container(
                                  height: 70,
                                  padding: const EdgeInsets.only(left: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Row(
                                        children: [
                                          Text(
                                            chapter.chapterName.toString(),
                                            style: GoogleFonts.poppins(
                                              fontSize: 16,
                                              fontWeight: FontWeight.w600,
                                              height: 1.4,
                                              color: blackColor,
                                            ),
                                          ),
                                          const Spacer(),
                                          Container(
                                            padding: const EdgeInsets.all(5),
                                            decoration: BoxDecoration(
                                                color: lightSky,
                                                shape: BoxShape.circle),
                                            child: Icon(
                                              Icons.bookmark_border,
                                              color: lightBlue,
                                              size: 15,
                                            ),
                                          )
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 15,
                                      ),
                                      Text(
                                        chapter.chapterDes.toString(),
                                        style: GoogleFonts.poppins(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500,
                                          height: 1.25,
                                          color: textField,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Divider(
                            height: 2,
                            thickness: 1,
                            color: borderColor,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    color: brightRed,
                                    borderRadius: BorderRadius.circular(10)),
                                padding: const EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 10),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      '4.8',
                                      style: GoogleFonts.poppins(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                        color: lightRed,
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Icon(
                                      Icons.star,
                                      color: lightRed,
                                      size: 14,
                                    )
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 3,
                                child: InkWell(
                                  onTap:(){
                                    // Navigator.pushNamed(context, RoutesName.courseStart);
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => CourseStart(chapter.bookID!, chapter.chapterId!,chapter.chapterName!, chapter.chapterNotes!, chapter.chapterVideo!),));
                                    // Navigator.push(context, MaterialPageRoute(builder: (context) => YoutubePlayerDemoApp(),));
                                    },
                                  child: Container(
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.only(left: 10),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 5, horizontal: 10),
                                    decoration: BoxDecoration(
                                      color: lightSky,
                                      borderRadius: BorderRadius.circular(50),
                                    ),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          'Start',
                                          style: GoogleFonts.poppins(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500,
                                            color: lightBlue,
                                          ),
                                        ),
                                        const SizedBox(
                                          width: 5,
                                        ),
                                        Icon(
                                          Icons.arrow_forward,
                                          size: 16,
                                          color: lightBlue,
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ));
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
  List<ChaptersResult> allChaptersList = [];

  Future<void> getAllChapters() async {
    AppLoaderProgress.showLoader(context);
    final response = await http.post(
      Uri.parse(UtilsMethod.baseUrl),
      body: {
        "api_name": "getAllChapters",
        "bookName": widget.bookName,
      },
    );
    if (response.statusCode == 200) {
      allChaptersList =
      ChaptersModeals.fromJson(jsonDecode(response.body)).resonse!.result!;
      setState(() {});
    }

    AppLoaderProgress.hideLoader(context);
  }
}
