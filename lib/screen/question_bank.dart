import 'dart:async';
import 'dart:convert';
import 'package:cosmolearn/appUtilities/app_constants.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import '../appUtilities/app_circular_loader.dart';
import '../appUtilities/app_colors.dart';
import '../appUtilities/utils.dart';
import '../view_models/question_models.dart';

class QuestionBank extends StatefulWidget {
  String bookId;
  String chapterId;
  QuestionBank(this.bookId, this.chapterId);

  @override
  State<QuestionBank> createState() => _QuestionBankState();
}

class _QuestionBankState extends State<QuestionBank> {
  int currentQuestionIndex = 0;
  Color selectedBorderColor = greenColor;
  Color unselectedBorderColor = subText;

  String selectedIndex = '';
  final PageController _pageController = PageController();

  void selectAnswer() {
    if (selectedIndex.isNotEmpty) {
      if (currentQuestionIndex < questionList.length - 1) {
        _resetTimer();
        _startTimer();

        setState(() {
          selectedIndex = '';
          currentQuestionIndex++;
        });
      }
    }else{
      Fluttertoast.showToast(msg: "Please choose Answer first");
    }
  }

  void skipAnswer() {
    if (selectedIndex.isEmpty) {
      if (currentQuestionIndex < questionList.length - 1) {
        _resetTimer();
        _startTimer();
        // _resetTimer();
        setState(() {
          selectedIndex = '';
          currentQuestionIndex++;
        });
      }
    }
  }

  int _seconds = 0;
  late Timer _timer;

  String _formatDuration(int seconds) {
    Duration duration = Duration(seconds: seconds);
    return "${duration.inMinutes.remainder(60).toString().padLeft(2, '0')}min : ${(duration.inSeconds % 60).toString().padLeft(2, '0')}sec";
  }

  void _startTimer() {
    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      setState(() {
        _seconds++;
      });
    });
  }

  void _pauseTimer() {
    _timer.cancel();
  }

  void _resetTimer() {
    _timer.cancel();
    setState(() {
      _seconds = 0;
    });
  }

  @override
  void initState() {
    // TODO: implement initState

    Future.delayed(Duration.zero, () {
      getAllQuestion();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: mainColor,
        appBar: AppBar(
          iconTheme: const IconThemeData(
            color: Colors.black, //change your color here
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
          title: Text(
            'Question Bank',
            style: GoogleFonts.poppins(
                fontSize: 16, fontWeight: FontWeight.w600, color: blackColor),
          ),
        ),
        body: PageView.builder(
          controller: _pageController,
          itemCount: questionList.length,
          physics: const NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return Stack(
              children: [
                SingleChildScrollView(
                  child: Container(
                    margin: const EdgeInsets.symmetric(horizontal: 20),
                    child: ListView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: 1,
                      itemBuilder: (context, index) {
                        QuestionResult Qus = questionList[currentQuestionIndex];
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(height: 10.0),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.watch_later_outlined,
                                  size: 18,
                                  color: primaryColor,
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  'Time: ${_formatDuration(_seconds)}',
                                  style:
                                      TextStyle(fontSize: 14, color: lightRed),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 15,
                            ),
                            Text(
                              Qus.question.toString(),
                              style: GoogleFonts.poppins(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.w500,
                                  height: 1.7),
                              textAlign: TextAlign.start,
                            ),
                            Image.network(
                              Qus.questionImage.toString(),
                              fit: BoxFit.fill,
                              width: MediaQuery.of(context).size.width,
                              height: 110,
                              errorBuilder: (context, error, stackTrace) {
                                return Container();
                              },
                            ),
                            const SizedBox(height: 15.0),
                            ListView.builder(
                              physics: const NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemCount: 1,
                              itemBuilder: (context, answerIndex) {
                                QuestionResult Ans =
                                    questionList[currentQuestionIndex];

                                return Column(
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        if (selectedIndex.isEmpty) {
                                          selectedIndex = 'A';
                                          setState(() {});
                                        }
                                      },
                                      child: Container(
                                        margin: const EdgeInsets.symmetric(
                                            vertical: 8),
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 20, vertical: 15),
                                        decoration: BoxDecoration(
                                          boxShadow: [
                                            BoxShadow(
                                              color: borderColor,
                                              blurRadius: 4,
                                              offset: const Offset(
                                                  2, 4), // Shadow position
                                            ),
                                          ],
                                          border: Border.all(
                                            color: selectedIndex.isNotEmpty
                                                ? selectedIndex == "A"
                                                    ? selectedIndex ==
                                                            Ans.correctAnswer
                                                        ? Colors.green
                                                        : Colors.red
                                                    : 'A' == Ans.correctAnswer
                                                        ? Colors.green
                                                        : borderColor
                                                : borderColor,
                                            width: 1.5,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: Colors.white,
                                        ),
                                        child: Column(
                                          children: [
                                            Row(
                                              children: [
                                                Expanded(
                                                  child: Text(
                                                    Qus.optionA.toString(),
                                                    textAlign: TextAlign.start,
                                                    style: TextStyle(
                                                      color: selectedIndex
                                                              .isNotEmpty
                                                          ? selectedIndex == "A"
                                                              ? selectedIndex ==
                                                                      Ans.correctAnswer
                                                                  ? Colors.green
                                                                  : Colors.red
                                                              : 'A' == Ans.correctAnswer
                                                                  ? Colors.green
                                                                  : Colors.black
                                                          : Colors.black,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Image.network(
                                              Qus.optionAImage.toString(),
                                              fit: BoxFit.fill,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              height: 110,
                                              errorBuilder:
                                                  (context, error, stackTrace) {
                                                return Container();
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        if (selectedIndex.isEmpty) {
                                          selectedIndex = 'B';
                                          _pauseTimer();
                                          setState(() {});
                                        }
                                      },
                                      child: Container(
                                        margin: const EdgeInsets.symmetric(
                                            vertical: 8),
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 20, vertical: 15),
                                        decoration: BoxDecoration(
                                          boxShadow: [
                                            BoxShadow(
                                              color: borderColor,
                                              blurRadius: 4,
                                              offset: Offset(2, 4), // Shadow position
                                            ),],
                                          border: Border.all(
                                            color: selectedIndex.isNotEmpty
                                                ? selectedIndex == "B"
                                                    ? selectedIndex ==
                                                            Ans.correctAnswer
                                                        ? Colors.green
                                                        : Colors.red
                                                    : 'B' == Ans.correctAnswer
                                                        ? Colors.green
                                                        : borderColor
                                                : borderColor,
                                            width: 1.5,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: Colors.white,
                                        ),
                                        child: Column(
                                          children: [
                                            Row(
                                              children: [
                                                Expanded(
                                                  child: Text(
                                                    Qus.optionB.toString(),
                                                    textAlign: TextAlign.start,
                                                    style: TextStyle(
                                                      color: selectedIndex
                                                              .isNotEmpty
                                                          ? selectedIndex == "B"
                                                              ? selectedIndex ==
                                                                      Ans.correctAnswer
                                                                  ? Colors.green
                                                                  : Colors.red
                                                              : 'B' == Ans.correctAnswer
                                                                  ? Colors.green
                                                                  : Colors.black
                                                          : Colors.black,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Image.network(
                                              Qus.optionBImage.toString(),
                                              fit: BoxFit.fill,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              height: 110,
                                              errorBuilder:
                                                  (context, error, stackTrace) {
                                                return Container();
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        if (selectedIndex.isEmpty) {
                                          selectedIndex = 'C';
                                          _pauseTimer();
                                          setState(() {});
                                        }
                                      },
                                      child: Container(
                                        margin: const EdgeInsets.symmetric(
                                            vertical: 8),
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 20, vertical: 15),
                                        decoration: BoxDecoration(
                                          boxShadow: [
                                            BoxShadow(
                                              color: borderColor,
                                              blurRadius: 4,
                                              offset: const Offset(2, 4), // Shadow position
                                            ),],
                                          border: Border.all(
                                            color: selectedIndex.isNotEmpty
                                                ? selectedIndex == "C"
                                                    ? selectedIndex ==
                                                            Ans.correctAnswer
                                                        ? Colors.green
                                                        : Colors.red
                                                    : 'C' == Ans.correctAnswer
                                                        ? Colors.green
                                                        : borderColor
                                                : borderColor,
                                            width: 1.5,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: Colors.white,
                                        ),
                                        child: Column(
                                          children: [
                                            Row(
                                              children: [
                                                Expanded(
                                                  child: Text(
                                                    Qus.optionC.toString(),
                                                    textAlign: TextAlign.start,
                                                    style: TextStyle(
                                                      color: selectedIndex
                                                              .isNotEmpty
                                                          ? selectedIndex == "C"
                                                              ? selectedIndex ==
                                                                      Ans.correctAnswer
                                                                  ? Colors.green
                                                                  : Colors.red
                                                              : 'C' == Ans.correctAnswer
                                                                  ? Colors.green
                                                                  : Colors.black
                                                          : Colors.black,
                                                    ),
                                                  ),
                                                ),

                                              ],
                                            ),
                                            Image.network(
                                              Qus.optionCImage.toString(),
                                              fit: BoxFit.fill,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              height: 110,
                                              errorBuilder:
                                                  (context, error, stackTrace) {
                                                return Container();
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        if (selectedIndex.isEmpty) {
                                          selectedIndex = 'D';
                                          _pauseTimer();
                                          setState(() {});
                                        }
                                      },
                                      child: Container(
                                        margin: const EdgeInsets.symmetric(
                                            vertical: 8),
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 20, vertical: 15),
                                        decoration: BoxDecoration(
                                          boxShadow: [
                                            BoxShadow(
                                              color: borderColor,
                                              blurRadius: 4,
                                              offset: const Offset(2, 4), // Shadow position
                                            ),],
                                          border: Border.all(
                                            color: selectedIndex.isNotEmpty
                                                ? selectedIndex == "D"
                                                    ? selectedIndex ==
                                                            Ans.correctAnswer
                                                        ? Colors.green
                                                        : Colors.red
                                                    : 'D' == Ans.correctAnswer
                                                        ? Colors.green
                                                        : borderColor
                                                : borderColor,
                                            width: 1.5,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: Colors.white,
                                        ),
                                        child: Column(
                                          children: [
                                            Row(
                                              children: [
                                                Expanded(
                                                  child: Text(
                                                    Qus.optionD.toString(),
                                                    textAlign: TextAlign.start,
                                                    style: TextStyle(
                                                      color: selectedIndex
                                                              .isNotEmpty
                                                          ? selectedIndex == "D"
                                                              ? selectedIndex ==
                                                                      Ans.correctAnswer
                                                                  ? Colors.green
                                                                  : Colors.red
                                                              : 'D' == Ans.correctAnswer
                                                                  ? Colors.green
                                                                  : Colors.black
                                                          : Colors.black,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Image.network(
                                              Qus.optionDImage.toString(),
                                              fit: BoxFit.fill,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              height: 110,
                                              errorBuilder:
                                                  (context, error, stackTrace) {
                                                return Container();
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                );
                              },
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            selectedIndex.isNotEmpty
                                ? Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        AppConstants.answerExplanation,
                                        style: GoogleFonts.poppins(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w600,
                                          color: lightBlue,
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      Image.network(
                                        Qus.aunswerImage.toString(),
                                        fit: BoxFit.fill,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        height: 110,
                                        errorBuilder:
                                            (context, error, stackTrace) {
                                          return Container();
                                        },
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      Text(
                                        Qus.explaination.toString(),
                                        style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.w400,
                                          height: 1.4,
                                          color: subText,
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                    ],
                                  )
                                : Container(),
                            const SizedBox(
                              height: 60,
                            )
                          ],
                        );
                      },
                    ),
                  ),
                ),
                Positioned(
                  bottom: 10,
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      children: [
                        Expanded(
                          child: InkWell(
                            onTap: () {
                              skipAnswer();
                              _pageController.animateToPage(
                                currentQuestionIndex,
                                duration: Duration(milliseconds: 300),
                                curve: Curves.easeInOut,
                              );
                            },
                            child: Container(
                              alignment: Alignment.center,
                              margin: const EdgeInsets.only(
                                  right: 10, left: 10, bottom: 10),
                              height: 40,
                              decoration: BoxDecoration(
                                color: lightBlue,
                                borderRadius: BorderRadius.circular(50),
                              ),
                              child: Text(
                                AppConstants.skip,
                                style: GoogleFonts.poppins(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                  height: 1.4,
                                  color: whiteColor,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: InkWell(
                            onTap: () {
                              selectAnswer();

                              _pageController.animateToPage(
                                currentQuestionIndex,
                                duration: Duration(milliseconds: 300),
                                curve: Curves.easeInOut,
                              );
                            },
                            child: Container(
                              alignment: Alignment.center,
                              margin: const EdgeInsets.only(
                                  right: 10, left: 10, bottom: 10),
                              height: 40,
                              decoration: BoxDecoration(
                                color: lightBlue,
                                borderRadius: BorderRadius.circular(50),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    AppConstants.next,
                                    style: GoogleFonts.poppins(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600,
                                      height: 1.4,
                                      color: whiteColor,
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  Icon(
                                    Icons.arrow_forward,
                                    size: 16,
                                    color: whiteColor,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            );
          },
        ));
  }

  List<QuestionResult> questionList = [];

  Future<void> getAllQuestion() async {
    AppLoaderProgress.showLoader(context);
    final response = await http.post(
      Uri.parse(UtilsMethod.baseUrl),
      body: {
        "api_name": "getQuestionBank",
        "bookId": widget.bookId,
        "chapterId": widget.chapterId,
      },
    );
    if (response.statusCode == 200) {
      questionList =
          QuestionModeals.fromJson(jsonDecode(response.body)).resonse!.result!;
      _startTimer();
      // setState(() {});
    }

    AppLoaderProgress.hideLoader(context);
  }
}
