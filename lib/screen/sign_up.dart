import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import '../appUtilities/AppPrefrences.dart';
import '../appUtilities/app_circular_loader.dart';
import '../appUtilities/app_colors.dart';
import '../appUtilities/app_constants.dart';
import '../appUtilities/utils.dart';
import 'otp_screen.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  TextEditingController name = TextEditingController();
  TextEditingController mobileNumber = TextEditingController();
  TextEditingController emailID = TextEditingController();

  bool isEmail(String input) => RegExp(
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
      .hasMatch(input);

  bool isPhone(String input) =>
      RegExp(r'^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$')
          .hasMatch(input);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          backgroundColor: mainColor,
          body: Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              color: mainColor,
            ),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 40),
                    child: Image.asset(
                      'assets/images/logo.png',
                      fit: BoxFit.fill,
                      height: 70,
                    ),
                  ),
                  Text(
                    AppConstants.sign_Up,
                    style: GoogleFonts.poppins(
                      fontSize: 25,
                      fontWeight: FontWeight.w600,
                      height: 1.25,
                      color: blackColor,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    AppConstants.loginPageText,
                    style: GoogleFonts.poppins(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      height: 1.6,
                      color: subText,
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Container(
                    height: 50,
                    margin: const EdgeInsets.only(top: 10, bottom: 10),
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 9),
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(5),
                        ),
                        color: Colors.white),
                    child: TextField(
                      controller: name,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: AppConstants.enterName,
                        hintStyle: GoogleFonts.poppins(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          height: 1.25,
                          color: textField,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: 50,
                    margin: const EdgeInsets.only(top: 10, bottom: 10),
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 9),
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(5),
                        ),
                        color: Colors.white),
                    child: TextField(
                      controller: mobileNumber,
                      keyboardType: TextInputType.number,
                      inputFormatters: [
                        FilteringTextInputFormatter.digitsOnly // FilteringTextInputFormatter.digitsOnly
                      ],
                      maxLength: 10,
                      decoration: InputDecoration(
                        counterText: '',
                        border: InputBorder.none,
                        hintText: AppConstants.enterMobile,
                        hintStyle: GoogleFonts.poppins(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          height: 1.25,
                          color: textField,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: 50,
                    margin: const EdgeInsets.only(top: 10, bottom: 10),
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 9),
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(5),
                        ),
                        color: Colors.white),
                    child: TextField(
                      controller: emailID,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: AppConstants.enterYourEmail,
                        hintStyle: GoogleFonts.poppins(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          height: 1.25,
                          color: textField,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Material(
                    color: lightRed,
                    borderRadius: BorderRadius.circular(50),
                    child: InkWell(
                      borderRadius: BorderRadius.circular(50),
                      onTap: () {
                        if(name.text.toString().isEmpty){
                          Fluttertoast.showToast(msg: 'Please Enter Full Name');
                        } else if (mobileNumber.text.toString().isEmpty) {
                          Fluttertoast.showToast(msg: 'Please Enter Mobile Number');
                        }else if(!isPhone(mobileNumber.text.toString())){
                          Fluttertoast.showToast(msg: 'Please Enter Valid Mobile Number');
                        } else if (!isEmail(emailID.text.toString())) {
                          Fluttertoast.showToast(msg: 'Please Enter Valid Email Id');
                        } else if (emailID.text.toString().isEmpty) {
                          Fluttertoast.showToast(
                              msg: 'Please Enter Email Id');
                        }else {
                          postData().then((response){
                            var data=jsonDecode(response)["resonse"];
                            if(data['status']==200){

                              AppPrefrence.putString("mobile",mobileNumber.text.toString());
                              AppPrefrence.putString("emailId",emailID.text.toString());
                              Navigator.push(context, MaterialPageRoute(builder: (context) => otpScreen(mobileNumber.text.toString()),));
                            }
                            // Fluttertoast.showToast(msg: data['result']);
                          });
                        }
                      },
                      child: Container(
                        height: 50,
                        width: MediaQuery.of(context).size.width,
                        alignment: Alignment.center,
                        child: Text(
                          AppConstants.sign_Up,
                          style: GoogleFonts.poppins(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            height: 1.25,
                            color: whiteColor,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }

  Future<dynamic> postData() async {
    AppLoaderProgress.showLoader(context);
    final response = await http.post(Uri.parse(UtilsMethod.baseUrl),
      body: {
        "api_name": "signUp",
        "mobile":mobileNumber.text.toString(),
        "name":name.text.toString(),
        "email":emailID.text.toString()
      },
    );
    AppLoaderProgress.hideLoader(context);
    return response.body;
  }


}
