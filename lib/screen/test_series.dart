import 'dart:convert';

import 'package:cosmolearn/appUtilities/app_circular_loader.dart';
import 'package:cosmolearn/appUtilities/routes/routes_name.dart';
import 'package:cosmolearn/appUtilities/utils.dart';
import 'package:cosmolearn/screen/test_series_details.dart';
import 'package:cosmolearn/view_models/testseries_models.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import '../appUtilities/AppPrefrences.dart';
import '../appUtilities/app_colors.dart';
import '../appUtilities/app_constants.dart';
import '../paymentGateway/buy_now.dart';

class TestSeries extends StatefulWidget {
  const TestSeries({super.key});

  @override
  State<TestSeries> createState() => _TestSeriesState();
}

class _TestSeriesState extends State<TestSeries> {
  @override

  void initState() {
    // TODO: implement initState
    Future.delayed(Duration.zero, () {
      testSeriesData();
    });
    super.initState();
  }

  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: mainColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: const IconThemeData(
            color: Colors.black, //change your color here
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
          title: Padding(
            padding: const EdgeInsets.only(left: 8),
            child: Text(
              'Test Series',
              style: GoogleFonts.poppins(fontSize: 18,fontWeight: FontWeight.w600,color: blackColor),
            ),
          ),
        ),
        body: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      alignment: Alignment.centerLeft,
                      child: ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: getTestSeriesData.length,
                        scrollDirection: Axis.vertical,
                        itemBuilder: (context, index) {

                          TestSeriesResult test = getTestSeriesData[index];

                          return Container(
                            padding: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  color: whiteColor,
                                  borderRadius: BorderRadius.circular(10)),
                              margin: EdgeInsets.all(10),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Expanded(
                                    flex: 1,
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: Image.network(
                                        test.image.toString(),
                                        fit: BoxFit.cover,
                                        height: 80,
                                        errorBuilder:
                                            (BuildContext context,
                                            Object exception,
                                            StackTrace?
                                            stackTrace) {
                                          return Image.asset(
                                            'assets/images/discoverbanner.png',
                                            fit: BoxFit.cover,
                                            height: 80,
                                          ); // Display the fallback asset image
                                        },
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 3,
                                    child: Container(
                                      height: 80,
                                      padding: EdgeInsets.only(left: 10),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          InkWell(onTap:(){
                                            Navigator.push(context, MaterialPageRoute(builder: (context) => TestSeriesDetails(test.testType.toString()),));
                                          },
                                            child: Row(
                                              children: [
                                                Text(
                                                  test.testType.toString(),
                                                  style: GoogleFonts.poppins(
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.w500,
                                                    height: 1.4,
                                                    color: blackColor,
                                                  ),
                                                ),
                                                const Spacer(),
                                              const Icon(Icons.arrow_forward_ios,size: 16,)
                                              ],
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 15,
                                          ),
                                          Row(
                                            children: [
                                              test.offer != "0"?Row(children: [Image.asset('assets/images/off.png',height: 16,),
                                                const SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  test.offer.toString(),
                                                  style: GoogleFonts.poppins(
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w500,
                                                    height: 1.25,
                                                    color: subText,
                                                  ),
                                                ),],): Container(),
                                              const Spacer(),
                                              Row(
                                                children: [
                                                  Container(
                                                    decoration: BoxDecoration(
                                                        color: brightRed,
                                                        borderRadius:
                                                        BorderRadius.circular(
                                                            10)),
                                                    padding: const EdgeInsets.symmetric(
                                                        vertical: 5,
                                                        horizontal: 15),
                                                    child: Row(children: [
                                                      Text(
                                                        AppConstants
                                                            .rupeesSymbol,
                                                        style:
                                                        GoogleFonts.poppins(
                                                          fontSize: 12,
                                                          fontWeight:
                                                          FontWeight.w500,
                                                          height: 1.25,
                                                          color: lightRed,
                                                        ),
                                                      ),
                                                      const SizedBox(
                                                        width: 5,
                                                      ),
                                                      Text(
                                                        test.price.toString(),
                                                        style:
                                                        GoogleFonts.poppins(
                                                          fontSize: 12,
                                                          fontWeight:
                                                          FontWeight.w500,
                                                          height: 1.25,
                                                          color: lightRed,
                                                        ),
                                                      ),
                                                    ]),
                                                  ),
                                                  const SizedBox(
                                                    width: 10,
                                                  ),
                                                  InkWell(onTap: (){
                                                    if((test.price??'0').isNotEmpty){
                                                      String price = (int.parse(test.price.toString().split("/-")[0]) -
                                                          ((int.parse(test.price.toString().split("/-")[0]) *
                                                              int.parse(test.offer.toString().split("%")[0])) /
                                                              100))
                                                          .toString();

                                                      Navigator.push(context, MaterialPageRoute(builder: (context) => BuyNowPage(price,test.testid.toString(), test.description.toString(),test.image.toString()),));
                                                    }else{
                                                      Fluttertoast.showToast(msg: 'Invalid amount');
                                                    }

                                                  },
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                          color: lightSky,
                                                          borderRadius:
                                                          BorderRadius.circular(
                                                              10)),
                                                      padding: const EdgeInsets.symmetric(
                                                          vertical: 5,
                                                          horizontal: 15),
                                                      child: Text(
                                                        AppConstants.buy,
                                                        style: GoogleFonts.poppins(
                                                          fontSize: 12,
                                                          fontWeight: FontWeight.w500,
                                                          height: 1.25,
                                                          color: lightBlue,
                                                        ),
                                                      ),
                                                    ),
                                                  ),

                                                ],
                                              )

                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ));
                        },
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  List<TestSeriesResult> getTestSeriesData = [];
  Future<void> testSeriesData() async {
    AppLoaderProgress.showLoader(context);
    String ClientId = await AppPrefrence.getString("id");
    String TeacherId = await AppPrefrence.getString("teacher_id");
      print("ClientId $ClientId");
      print("TeacherId $TeacherId");
    final response = await http.post(Uri.parse(UtilsMethod.baseUrl),
      body: {
      "api_name" : "getTestTypes",
      "client_id" : ClientId,
      "teacher_id" : TeacherId,
      }
    );
    if(response.statusCode == 200){
      var Response = jsonDecode(response.body);
      getTestSeriesData = TestSeriesModels.fromJson(Response).resonse!.result!;
      setState(() {

      });
    }
    AppLoaderProgress.hideLoader(context);
  }

}
