import 'package:cosmolearn/appUtilities/app_constants.dart';
import 'package:cosmolearn/screen/notes_screen.dart';
import 'package:cosmolearn/screen/question_bank.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import '../appUtilities/app_colors.dart';

class CourseStart extends StatefulWidget {
  String bookId;
  String chapterId;
  String chapterName;
  String chapterNotes;
  String chapterVideo;
  CourseStart(this.bookId, this.chapterId, this.chapterName, this.chapterNotes, this.chapterVideo);

  @override
  State<CourseStart> createState() => _CourseStartState();
}

class _CourseStartState extends State<CourseStart> with WidgetsBindingObserver {
  YoutubePlayer? player;
  int chooesOption = 0;
  int currentQuestionIndex = 0;
  late YoutubePlayerController _controller;
  late TextEditingController _idController;
  late TextEditingController _seekToController;
  late PlayerState _playerState;
  late YoutubeMetaData _videoMetaData;
  // double _volume = 100;
  bool muted = false;
  bool _isPlayerReady = false;


  void initState() {
    super.initState();
    _controller = YoutubePlayerController(
      initialVideoId: widget.chapterVideo,
      flags: const YoutubePlayerFlags(
        mute: false,
        autoPlay: true,
        disableDragSeek: false,
        loop: false,
        isLive: false,
        forceHD: false,
        enableCaption: true,
      ),
    )..addListener(listener);
    _idController = TextEditingController();
    _seekToController = TextEditingController();
    _videoMetaData = const YoutubeMetaData();
    _playerState = PlayerState.unknown;
  }

  void listener() {
    if (_isPlayerReady && mounted && !_controller.value.isFullScreen) {
      setState(() {
        _playerState = _controller.value.playerState;
        _videoMetaData = _controller.metadata;
      });
    }
  }

  @override
  void deactivate() {
    // Pauses video while navigating to next page.
    _controller.pause();
    super.deactivate();
  }

  @override
  void dispose() {
    _controller.dispose();
    _idController.dispose();
    _seekToController.dispose();
    super.dispose();
  }




  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: mainColor,
      appBar: AppBar(
        iconTheme: const IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text(
          widget.chapterName,
          style: GoogleFonts.poppins(
              fontSize: 16,
              fontWeight: FontWeight.w600,
              color: blackColor),
        ),
      ),
      body: Column(
        children: [
          DefaultTabController(
            length: 3,
            child: Container(
              decoration: BoxDecoration(
                  color: whiteColor,
                  borderRadius: const BorderRadius.all(Radius.circular(40))),
              child: TabBar(


                padding: EdgeInsets.zero,
                indicatorPadding: EdgeInsets.zero,
                indicator: const BoxDecoration(
                    color: Colors.transparent,
                    borderRadius: BorderRadius.all(Radius.circular(40))),
                indicatorColor: Colors.white,
                tabs: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: Text(
                      AppConstants.video,
                      style: GoogleFonts.poppins(
                          fontSize: 11,
                          fontWeight: FontWeight.w400,
                          color: lightBlue),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      _controller.pause();
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => QuestionBank(
                                widget.bookId, widget.chapterId),
                          ));
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Text(
                        AppConstants.questionBank,
                        style: GoogleFonts.poppins(
                            fontSize: 11,
                            fontWeight: FontWeight.w400,
                            color: lightBlue),
                      ),
                    ),
                  ),
                  InkWell(

                    onTap: () {
                      _controller.pause();
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => NotesPage(
                                widget.chapterName, widget.chapterNotes),
                          ));
                    },

                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Text(
                        AppConstants.notes,
                        style: GoogleFonts.poppins(
                            fontSize: 11,
                            fontWeight: FontWeight.w400,
                            color: lightBlue),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          SizedBox(
            height: 200,
            width: MediaQuery.of(context).size.width,
            child: YoutubePlayerBuilder(
                onExitFullScreen: () {
                  // The player forces portraitUp after exiting fullscreen. This overrides the behaviour.
                  SystemChrome.setPreferredOrientations(DeviceOrientation.values);
                },
                player: YoutubePlayer(
                  controller: _controller,
                  showVideoProgressIndicator: true,
                  onReady: () {
                    _isPlayerReady = true;
                  },
                ),
                builder: (subContext, player){
                  return  player;
                }
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 20,
                ),
                Text(
                  'Course Name',
                  style: GoogleFonts.poppins(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                      color: blackColor),
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Icon(
                          Icons.play_circle_outline,
                          size: 18,
                          color: lightRed,
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Text(
                          '15 Questions',
                          style: GoogleFonts.poppins(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                            height: 1.25,
                            color: textField,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.watch_later_outlined,
                          size: 18,
                          color: lightRed,
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Text(
                          '30 Min.',
                          style: GoogleFonts.poppins(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                            height: 1.25,
                            color: textField,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.file_open_outlined,
                          size: 18,
                          color: lightRed,
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Text(
                          '2 resources',
                          style: GoogleFonts.poppins(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                            height: 1.25,
                            color: textField,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Row(
                      children: [
                        Text(
                          '4.8',
                          style: GoogleFonts.poppins(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            height: 1.25,
                            color: lightRed,
                          ),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Text(
                          '★★★★★',
                          style: GoogleFonts.poppins(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                            height: 1.25,
                            color: textField,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  'With cutting-edge technology and insightful analytics, Cos-moLearn ensures personalized learning pathways, enabling you to identify your strengths and weaknesses, thus optimiz-ing your performance. ',
                  style: GoogleFonts.poppins(
                      fontSize: 14,
                      fontWeight: FontWeight.normal,
                      color: subText),
                ),
                const SizedBox(
                  height: 10,
                ),
                Divider(
                  height: 2,
                  color: borderColor,
                  thickness: 2,
                ),
                const SizedBox(
                  height: 10,
                ),
                InkWell(
                  onTap: () {
                    // Navigator.push(context, MaterialPageRoute(builder: (context) => Practice(),));
                  },
                  child: Text(
                    'Course Details',
                    style: GoogleFonts.poppins(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        color: blackColor),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  'Language: English',
                  style: GoogleFonts.poppins(
                      fontSize: 14,
                      fontWeight: FontWeight.normal,
                      color: subText),
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  '24 video lessons, 35 hours material, 100% online studying at your own pace',
                  style: GoogleFonts.poppins(
                      fontSize: 14,
                      fontWeight: FontWeight.normal,
                      color: subText),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
