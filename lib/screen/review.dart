import 'package:cosmolearn/screen/result_screen.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../appUtilities/app_colors.dart';
import '../appUtilities/app_constants.dart';
import '../appUtilities/utils.dart';

class ReviewScreen extends StatefulWidget {

  String? chapterId;
  String? time;
  String? testTypeId;
  int? position;
  String? id;
  ReviewScreen({required this.chapterId,required this.time,required this.testTypeId,required this.position,required this.id,});
  @override
  State<ReviewScreen> createState() => _ReviewScreenState();
}

class _ReviewScreenState extends State<ReviewScreen> {
  showSubmitPopup() {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            actions: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                    child: Text(
                      "Do you want to Submit this Test?",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.lexend(
                          color: primaryColor,
                          fontWeight: FontWeight.w500,
                          fontSize: 16),
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        height: 30,
                        width: 72,
                        decoration: BoxDecoration(
                            color: brightRed,
                            borderRadius: BorderRadius.circular(100)),
                        child: InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Center(
                            child: Text(
                              "No",
                              style: GoogleFonts.poppins(
                                  color: lightRed,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(width: 10),
                      Container(
                        height: 30,
                        width: 72,
                        decoration: BoxDecoration(
                            color: lightSky,
                            borderRadius: BorderRadius.circular(100)),
                        child: InkWell(
                          onTap: () {
                            Navigator.pop(context);
                            // Navigator.push(context, MaterialPageRoute(builder: (context) => resultPage(widget.chapterId, widget.id),));
                          },
                          child: Center(
                            child: Text(
                              "Yes",
                              style: GoogleFonts.poppins(
                                  color: lightBlue,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 14),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(width: 20),
                    ],
                  ),
                ],
              ),
            ],
            actionsPadding: const EdgeInsets.only(bottom: 15),
          );
        });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: mainColor,
      appBar: AppBar(
        iconTheme: const IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text(
          'Review',
          style: GoogleFonts.poppins(
              fontSize: 16, fontWeight: FontWeight.w600, color: blackColor),
        ),
      ),
      body: Stack(
        alignment: Alignment.bottomRight,
        children: [
          SingleChildScrollView(padding: EdgeInsets.zero,
            child: Column(children: [
              const SizedBox(height: 20,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                        child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          height: 25,
                          width: 25,
                          decoration: const BoxDecoration(
                              shape: BoxShape.circle, color: Colors.green),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Text(
                          'ATTEMPTED',
                          style: GoogleFonts.poppins(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: blackColor),
                        ),
                      ],
                    )),
                    Expanded(
                        child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          height: 25,
                          width: 25,
                          decoration: const BoxDecoration(
                              shape: BoxShape.circle, color: Colors.red),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Text(
                          'NOT ATTEMPTED',
                          style: GoogleFonts.poppins(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: blackColor),
                        ),
                      ],
                    )),
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              ListView.builder(
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: UtilsMethod.addQuestionList.length,
                itemBuilder: (context, index) {

                  return Container(
                    margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 8),
                    decoration: BoxDecoration(
                      border: Border.all(color: UtilsMethod.addQuestionList[index].flag == 'NOT_ATTEMPTED' ? Colors.red : Colors.green),
                      boxShadow: [
                        BoxShadow(
                          color: borderColor,
                          blurRadius: 4,
                          offset: const Offset(4, 8), // Shadow position
                        ),
                      ],
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white,
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          padding: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            color: lightSky,
                            shape: BoxShape.circle,
                          ),
                          child: Text((index + 1).toString()),
                        ),
                        const SizedBox(width: 10),
                        Expanded(
                          child: Text(
                            UtilsMethod.addQuestionList[index].q_name.toString(),
                            textAlign: TextAlign.start,
                            style: TextStyle(color: blackColor),
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
              const SizedBox(height: 60,)
            ]),
          ),
          // InkWell(
          //   onTap: showSubmitPopup,
          //   child: Container(
          //     width: 120,
          //     alignment: Alignment.center,
          //     margin: EdgeInsets.only(right: 5, left: 5,bottom: 15),
          //     height: 40,
          //     decoration: BoxDecoration(
          //       color: lightblue,
          //       borderRadius: BorderRadius.circular(50),
          //     ),
          //     child: Text(
          //       AppConstants.submitButton,
          //       style: GoogleFonts.poppins(
          //         fontSize: 16,
          //         fontWeight: FontWeight.w600,
          //         height: 1.4,
          //         color: whiteColor,
          //       ),
          //     ),
          //   ),
          // ),
        ],
      ),
    );
  }

}
