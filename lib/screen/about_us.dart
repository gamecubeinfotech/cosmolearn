import 'package:cosmolearn/appUtilities/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../appUtilities/app_constants.dart';

class AboutUs extends StatefulWidget {
  const AboutUs({super.key});

  @override
  State<AboutUs> createState() => _AboutUsState();
}

class _AboutUsState extends State<AboutUs> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: mainColor,
      appBar: AppBar(
        iconTheme: const IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: lightSky,
        elevation: 0,
        title: Text(
          AppConstants.aboutUS,
          style: GoogleFonts.poppins(
              fontSize: 16, fontWeight: FontWeight.w600, color: blackColor),
        ),
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Column(
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  color: lightSky,
                  height: 160,
                  width: MediaQuery.of(context).size.width,
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 15),
                    width: 190,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Nayi Urja',
                          style: GoogleFonts.poppins(
                              fontSize: 24,
                              fontWeight: FontWeight.w600,
                              color: blackColor),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Text(
                          'Online Test Series Application',
                          style: GoogleFonts.poppins(
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              color: lightBlue),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 20,
                      ),
                      Text(
                        AppConstants.aboutUS,
                        style: GoogleFonts.poppins(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            color: lightRed),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Excel Beyond Limits: Master Your Success with Our Online Test Series!",
                        style: GoogleFonts.poppins(
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                            color: blackColor),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        AppConstants.aboutUsDescription,
                        style: GoogleFonts.poppins(
                            fontSize: 14,
                            fontWeight: FontWeight.normal,
                            color: subText),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        AppConstants.aboutUsDescription1,
                        style: GoogleFonts.poppins(
                            fontSize: 14,
                            fontWeight: FontWeight.normal,
                            color: subText),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        AppConstants.aboutUsDescription2,
                        style: GoogleFonts.poppins(
                            fontSize: 14,
                            fontWeight: FontWeight.normal,
                            color: subText),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        AppConstants.aboutUsDescription3,
                        style: GoogleFonts.poppins(
                            fontSize: 14,
                            fontWeight: FontWeight.normal,
                            color: subText),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Positioned(
                top: 20,
                right: 15,
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image.asset(
                      'assets/images/discoverbanner.png',
                      fit: BoxFit.cover,
                      height: 160,
                      width: 130,
                    ))),
          ],
        ),
      ),
    );
  }
}
