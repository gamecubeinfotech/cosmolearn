import 'dart:convert';

import 'package:cosmolearn/appUtilities/routes/routes_name.dart';
import 'package:cosmolearn/screen/start_test.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import '../appUtilities/AppPrefrences.dart';
import '../appUtilities/app_circular_loader.dart';
import '../appUtilities/app_colors.dart';
import '../appUtilities/app_constants.dart';
import '../appUtilities/utils.dart';
import '../view_models/test_type_model.dart';
import 'package:http/http.dart' as http;

class TestSeriesDetails extends StatefulWidget {
  String TestType = '';
  TestSeriesDetails(this.TestType);

  @override
  State<TestSeriesDetails> createState() => _TestSeriesDetailsState();
}

class _TestSeriesDetailsState extends State<TestSeriesDetails> {


  @override
  void initState() {
    // TODO: implement initState
    Future.delayed(Duration.zero, () {
      testTypeSeriesData();
    });
    super.initState();
  }

  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: mainColor,
        appBar: AppBar(
          iconTheme: const IconThemeData(
            color: Colors.black, //change your color here
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
          title: Text(
            widget.TestType,
            style: GoogleFonts.poppins(
                fontSize: 16, fontWeight: FontWeight.w600, color: blackColor),
          ),
        ),
        body: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(
                      height: 15,
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      alignment: Alignment.centerLeft,
                      child: ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: getTestTypeData.length,
                        scrollDirection: Axis.vertical,
                        itemBuilder: (context, index) {
                          TestSeriesNameResult typeTest =
                              getTestTypeData[index];
                          print('ttnameId' + typeTest.ttnameId.toString());
                          int Indexlist = index + 1;
                          return Container(
                              padding: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  color: whiteColor,
                                  borderRadius: BorderRadius.circular(10)),
                              margin: EdgeInsets.all(10),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Expanded(
                                    flex: 3,
                                    child: Container(
                                      height: 70,
                                      padding: EdgeInsets.only(right: 10),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Row(
                                            children: [
                                              Text(
                                                '$Indexlist.',
                                                style: GoogleFonts.poppins(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w600,
                                                  height: 1.4,
                                                  color: blackColor,
                                                ),
                                              ),
                                              const SizedBox(
                                                width: 5,
                                              ),
                                              Expanded(
                                                child: Text(
                                                  typeTest.testTypeName
                                                              .toString()
                                                              .length >
                                                          20
                                                      ? '${typeTest.testTypeName.toString().substring(0, 20)}...'
                                                      : typeTest.testTypeName
                                                          .toString(),
                                                  style: GoogleFonts.poppins(
                                                    fontSize: 16,
                                                    fontWeight: FontWeight.w600,
                                                    height: 1.4,
                                                    color: blackColor,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          const SizedBox(
                                            height: 15,
                                          ),
                                          Row(
                                            children: [
                                              Row(
                                                children: [
                                                  Icon(
                                                    Icons.play_circle_outline,
                                                    size: 18,
                                                    color: textField,
                                                  ),
                                                  const SizedBox(
                                                    width: 5,
                                                  ),
                                                  Text(
                                                    '${typeTest.noOfMcqs} Questions',
                                                    style: GoogleFonts.poppins(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      height: 1.25,
                                                      color: textField,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              const SizedBox(
                                                width: 10,
                                              ),
                                              Row(
                                                children: [
                                                  Icon(
                                                    Icons.watch_later_outlined,
                                                    size: 18,
                                                    color: textField,
                                                  ),
                                                  const SizedBox(
                                                    width: 5,
                                                  ),
                                                  Text(
                                                    typeTest.timeForTheTest
                                                        .toString(),
                                                    style: GoogleFonts.poppins(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      height: 1.25,
                                                      color: textField,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: typeTest.lockStatus != "LOCK"
                                        ? InkWell(
                                            onTap: () {
                                              if(typeTest.isAttemptedTest == "0"){
                                                showDialog(
                                                  barrierDismissible: false,
                                                  context: context,
                                                  builder: (context) {
                                                    return AlertDialog(
                                                      insetPadding: EdgeInsets.symmetric(horizontal: 15),
                                                      backgroundColor:
                                                      Colors.white,
                                                      shape:
                                                      RoundedRectangleBorder(
                                                        borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                      ),
                                                      actionsPadding:
                                                      EdgeInsets.zero,
                                                      actions: [
                                                        Column(
                                                          crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                          children: [
                                                            const SizedBox(
                                                              height: 5,
                                                            ),
                                                            Text(
                                                              typeTest.testTypeName.toString(),
                                                              style: GoogleFonts
                                                                  .poppins(
                                                                fontSize: 18,
                                                                fontWeight:
                                                                FontWeight
                                                                    .w600,
                                                                color: blackColor,
                                                              ),
                                                            ),
                                                            SizedBox(
                                                                width:
                                                                MediaQuery.of(
                                                                    context)
                                                                    .size
                                                                    .width,
                                                                child: Divider(
                                                                  color:
                                                                  blackColor,
                                                                  thickness: 1,
                                                                )),
                                                            const SizedBox(
                                                              height: 5,
                                                            ),
                                                            Padding(
                                                              padding:
                                                              const EdgeInsets
                                                                  .all(10),
                                                              child: Column(
                                                                crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                                children: [
                                                                  Row(
                                                                    mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .center,
                                                                    children: [
                                                                      Text(
                                                                        'Total No. of Question:',
                                                                        style: GoogleFonts
                                                                            .poppins(
                                                                          fontSize:
                                                                          18,
                                                                          fontWeight:
                                                                          FontWeight.w600,
                                                                          color:
                                                                          blackColor,
                                                                        ),
                                                                      ),
                                                                      SizedBox(
                                                                        width: 5,
                                                                      ),
                                                                      Text(
                                                                        typeTest.noOfMcqs.toString(),
                                                                        style: GoogleFonts
                                                                            .poppins(
                                                                          fontSize:
                                                                          16,
                                                                          fontWeight:
                                                                          FontWeight.w400,
                                                                          color:
                                                                          blackColor,
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                  const SizedBox(
                                                                    height: 10,
                                                                  ),
                                                                  Row(
                                                                    mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .center,
                                                                    children: [
                                                                      Text(
                                                                        'Time',
                                                                        style: GoogleFonts
                                                                            .poppins(
                                                                          fontSize:
                                                                          18,
                                                                          fontWeight:
                                                                          FontWeight.w600,
                                                                          color:
                                                                          blackColor,
                                                                        ),
                                                                      ),
                                                                      SizedBox(
                                                                        width: 5,
                                                                      ),
                                                                      Text(
                                                                        typeTest.timeForTheTest.toString(),
                                                                        style: GoogleFonts
                                                                            .poppins(
                                                                          fontSize:
                                                                          16,
                                                                          fontWeight:
                                                                          FontWeight.w400,
                                                                          color:
                                                                          blackColor,
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                  SizedBox(
                                                                    height: 10,
                                                                  ),
                                                                  Row(
                                                                    mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .center,
                                                                    children: [
                                                                      Text(
                                                                        'Nagative Marking:',
                                                                        style: GoogleFonts
                                                                            .poppins(
                                                                          fontSize:
                                                                          18,
                                                                          fontWeight:
                                                                          FontWeight.w600,
                                                                          color:
                                                                          blackColor,
                                                                        ),
                                                                      ),
                                                                      const SizedBox(
                                                                        width: 5,
                                                                      ),
                                                                      Text(
                                                                        typeTest.negativeMarks.toString(),
                                                                        style: GoogleFonts
                                                                            .poppins(
                                                                          fontSize:
                                                                          16,
                                                                          fontWeight:
                                                                          FontWeight.w400,
                                                                          color:
                                                                          blackColor,
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                  SizedBox(
                                                                    height: 5,
                                                                  ),
                                                                  Text(
                                                                    '(Negative Marking for Each Wrong Question 1)',
                                                                    textAlign:
                                                                    TextAlign
                                                                        .center,
                                                                    style: GoogleFonts
                                                                        .poppins(
                                                                      fontSize:
                                                                      14,
                                                                      fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                      color:
                                                                      blackColor,
                                                                    ),
                                                                  ),
                                                                  const SizedBox(
                                                                    height: 15,
                                                                  ),
                                                                  InkWell(
                                                                    onTap: (){

                                                                      // Navigator.pushNamed(context, RoutesName.startTest).then((value) => Navigator.pop(context));
                                                                      Navigator.pop(context);
                                                                      Navigator.push(context, MaterialPageRoute(builder: (context) => StartTest(typeTest.testTypeName,typeTest.ttnameId,typeTest.testTypeId,typeTest.timeForTheTest),));
                                                                    },
                                                                    child: Container(
                                                                      margin: EdgeInsets.symmetric(horizontal: 20),
                                                                      height: 35,
                                                                      width: MediaQuery.of(
                                                                          context)
                                                                          .size
                                                                          .width,
                                                                      alignment:
                                                                      Alignment
                                                                          .center,
                                                                      decoration: BoxDecoration(
                                                                          color: lightBlue,
                                                                          borderRadius:
                                                                          BorderRadius.circular(
                                                                              50)),
                                                                      child: Text(
                                                                        AppConstants
                                                                            .startTest,
                                                                        style: GoogleFonts
                                                                            .poppins(
                                                                          fontSize:
                                                                          16,
                                                                          fontWeight:
                                                                          FontWeight
                                                                              .w600,
                                                                          height:
                                                                          1.25,
                                                                          color:
                                                                          whiteColor,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  const SizedBox(
                                                                    height: 15,
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ],
                                                    );
                                                  },
                                                );
                                              }
                                              else {
                                                Fluttertoast.showToast(msg: 'You have already attempted this test');
                                              }
                                            },
                                            child: Container(
                                              alignment: Alignment.center,
                                              height: 70,
                                              decoration: BoxDecoration(
                                                color: lightBlue,
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                              ),
                                              child: Text(
                                                'Start',
                                                style: GoogleFonts.poppins(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w600,
                                                  height: 1.4,
                                                  color: whiteColor,
                                                ),
                                              ),
                                            ),
                                          )
                                        : Container(
                                            alignment: Alignment.center,
                                            height: 70,
                                            decoration: BoxDecoration(
                                                color: lightSky,
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                image: const DecorationImage(
                                                    image: AssetImage(
                                                        'assets/images/lock.png'),
                                                    fit: BoxFit.cover)),
                                          ),
                                  ),
                                ],
                              ));
                        },
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<TestSeriesNameResult> getTestTypeData = [];
  Future<void> testTypeSeriesData() async {
    AppLoaderProgress.showLoader(context);
    String ClientId = await AppPrefrence.getString("id");
    String TeacherId = await AppPrefrence.getString("teacher_id");
    String Mobile = await AppPrefrence.getString("mobile");

    final response = await http.post(Uri.parse(UtilsMethod.baseUrl), body: {
      "api_name": "getTestTypeNames",
      "testType": widget.TestType,
      "mobile": Mobile,
      "client_id": ClientId,
      "teacher_id": TeacherId,
    });
    if (response.statusCode == 200) {
      var Response = jsonDecode(response.body);
      getTestTypeData =
          TestSeriesNameModels.fromJson(Response).resonse!.result!;
      setState(() {});
    }
    AppLoaderProgress.hideLoader(context);
  }
}
