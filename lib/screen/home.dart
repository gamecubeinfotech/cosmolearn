import 'dart:convert';
import 'dart:io';

import 'package:cosmolearn/appUtilities/routes/routes.dart';
import 'package:cosmolearn/screen/report.dart';
import 'package:cosmolearn/screen/test_series.dart';
import 'package:cosmolearn/screen/test_series_details.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:carousel_slider/carousel_slider.dart';
import '../appUtilities/AppPrefrences.dart';
import '../appUtilities/app_circular_loader.dart';
import '../appUtilities/app_colors.dart';
import '../appUtilities/app_constants.dart';
import '../appUtilities/routes/routes_name.dart';
import 'package:http/http.dart' as http;
import '../appUtilities/utils.dart';
import '../paymentGateway/buy_now.dart';
import '../view_models/banner_models.dart';
import '../view_models/course_models.dart';
import 'course_details.dart';
import 'login_screen.dart';
import 'offer.dart';
import 'package:package_info/package_info.dart';


class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _currentIndex = 0;
  BannerModels bannerModels = BannerModels();
  CourseModels couserModels = CourseModels();
  int versionCode = 0;

  // PackageInfo _packageInfo = PackageInfo(
  //   appName: 'Unknown',
  //   packageName: 'Unknown',
  //   version: 'Unknown',
  //   buildNumber: 'Unknown',
  // );


  showExitPopup() {
    return showDialog(

        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            actions: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                    child: Text(
                      "Do you want to exit?",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.lexend(
                          color: primaryColor,
                          fontWeight: FontWeight.w500,
                          fontSize: 16),
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        height: 30,
                        width: 72,
                        decoration: BoxDecoration(
                            color: lightSky,
                            borderRadius: BorderRadius.circular(100)),
                        child: InkWell(
                          onTap: () {
                            SystemNavigator.pop();
                          },
                          child: Center(
                            child: Text(
                              "Yes",
                              style: GoogleFonts.poppins(
                                  color: lightBlue,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 14),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(width: 10),
                      Container(
                        height: 30,
                        width: 72,
                        decoration: BoxDecoration(
                            color: brightRed,
                            borderRadius: BorderRadius.circular(100)),
                        child: InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Center(
                            child: Text(
                              "No",
                              style: GoogleFonts.poppins(
                                  color: lightRed,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(width: 20),
                    ],
                  ),
                ],
              ),
            ],
            actionsPadding: const EdgeInsets.only(bottom: 15),
          );
        });
  }

  showUpdatePopup() {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {

        return WillPopScope(
          onWillPop: () async {
            return true ; },
          child: AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            actionsPadding: EdgeInsets.zero,
            actions: [
              Column(
                children: [
                  Container(
                    height: 250,
                    decoration: BoxDecoration(color: Colors.white,borderRadius: BorderRadius.circular(15),image:DecorationImage(image: AssetImage('assets/images/update_popup.png',),fit: BoxFit.cover),),
                  ),
                  Container(alignment: Alignment.center,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          AppConstants.newVersion,
                          style: GoogleFonts
                              .poppins(
                            fontSize: 18,
                            fontWeight:
                            FontWeight.w500,
                            height: 1.25,
                            color: lightBlue,
                          ),
                        ),
                        SizedBox(height: 10,),
                        Text(
                          AppConstants.versionDescription,
                          textAlign: TextAlign.center,
                          style: GoogleFonts
                              .poppins(
                            fontSize: 14,
                            fontWeight:
                            FontWeight.normal,
                            height: 1.25,
                            color: subText,
                          ),
                        ),
                        const SizedBox(height: 30,),
                        Container(
                          width: MediaQuery.of(context).size.width,

                          decoration: BoxDecoration(color: Colors.white,borderRadius: BorderRadius.circular(15),),
                          child: Container(
                            alignment: Alignment.center,
                            margin: const EdgeInsets.only(right: 50,left: 50,bottom: 20),
                            height: 40,
                            decoration: BoxDecoration(color: lightBlue,borderRadius: BorderRadius.circular(50),),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'Update',
                                  style: GoogleFonts.poppins(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600,
                                    height: 1.4,
                                    color: whiteColor,
                                  ),
                                ),
                                SizedBox(width: 10,),
                                Image.asset(
                                  'assets/images/arrow.png',
                                  height: 16,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),),
                ],
              )
            ],
          ),
        );
      },
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      getBannersList();
      getCourseList();
    });
  }

  Future<void> _initPackageInfo(int apiVersion) async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    versionCode =int.parse(info.buildNumber);

    if(versionCode < apiVersion){

      showUpdatePopup();
    }

  }

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
    return WillPopScope(
      onWillPop: () async {
        showExitPopup();
        return false;
      },
      child: SafeArea(
        child: Scaffold(
          key: scaffoldKey,
          backgroundColor: mainColor,
          drawer: Drawer(
            width: 270,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 20),
                        child: Row(
                          children: [
                            Image.asset(
                              'assets/images/logo.png',
                              fit: BoxFit.fill,
                              height: 25,
                            ),
                            const SizedBox(
                              width: 8,
                            ),
                            Text(
                              'Nayi Urja',
                              style: GoogleFonts.poppins(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w600,
                                  color: blackColor),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.pushNamed(context, RoutesName.aboutUs);
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              color: lightSky,
                              borderRadius: BorderRadius.circular(5)),
                          width: MediaQuery.of(context).size.width,

                          // padding: EdgeInsets.symmetric(vertical: 10),
                          margin: const EdgeInsets.symmetric(vertical: 5),
                          child: Row(
                            children: [
                              SizedBox(
                                  height: 45,
                                  child: VerticalDivider(
                                    width: 4,
                                    thickness: 4,
                                    color: lightBlue,
                                  )),
                              const SizedBox(
                                width: 10,
                              ),
                              Image.asset(
                                'assets/images/aboutUs.png',
                                height: 20,
                              ),
                              const SizedBox(
                                width: 20,
                              ),
                              Text(
                                'About Us',
                                style: GoogleFonts.poppins(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    color: blackColor),
                              ),
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.pushNamed(context, RoutesName.contactUs);
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              color: lightSky,
                              borderRadius: BorderRadius.circular(5)),
                          margin: const EdgeInsets.symmetric(vertical: 5),
                          child: Row(
                            children: [
                              SizedBox(
                                  height: 45,
                                  child: VerticalDivider(
                                    width: 4,
                                    thickness: 4,
                                    color: lightBlue,
                                  )),
                              const SizedBox(
                                width: 10,
                              ),
                              Image.asset(
                                'assets/images/contactUs.png',
                                height: 20,
                              ),
                              const SizedBox(
                                width: 20,
                              ),
                              Text(
                                'Contact Us',
                                style: GoogleFonts.poppins(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    color: blackColor),
                              ),
                            ],
                          ),
                        ),
                      ),

                      // InkWell(
                      //   onTap: () {
                      //     Navigator.pushNamed(context, RoutesName.courseDetail);
                      //   },
                      //   child: Container(
                      //     margin: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                      //     child: Text(
                      //       'Course Details',
                      //       style: GoogleFonts.poppins(fontSize: 20, color: blackColor),
                      //     ),
                      //   ),
                      // ),
                      // InkWell(
                      //   onTap: () {
                      //     Navigator.pushNamed(context, RoutesName.courseStart);
                      //   },
                      //   child: Container(
                      //     margin: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                      //     child: Text(
                      //       'Course Start',
                      //       style: GoogleFonts.poppins(fontSize: 20, color: blackColor),
                      //     ),
                      //   ),
                      // ),
                      // InkWell(
                      //   onTap: () {
                      //     Navigator.pushNamed(context, RoutesName.practice);
                      //   },
                      //   child: Container(
                      //     margin: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                      //     child: Text(
                      //       'Practice',
                      //       style: GoogleFonts.poppins(fontSize: 20, color: blackColor),
                      //     ),
                      //   ),
                      // ),
                      // InkWell(
                      //   onTap: () {
                      //     AppPrefrence.clearPrefrence();
                      //     Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => LoginScreen(),), (route) => false);
                      //   },
                      //   child: Container(
                      //     margin: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                      //     child: Text(
                      //       'Logout',
                      //       style: GoogleFonts.poppins(fontSize: 20, color: blackColor),
                      //     ),
                      //   ),
                      // ),
                    ],
                  ),
                  Column(
                    children: [
                      InkWell(
                        onTap: () {
                          AppPrefrence.clearPrefrence();
                          Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                builder: (context) => const LoginScreen(),
                              ),
                              (route) => false);
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              color: lightSky,
                              borderRadius: BorderRadius.circular(5)),
                          margin: const EdgeInsets.symmetric(vertical: 5),
                          child: Row(
                            children: [
                              SizedBox(
                                  height: 45,
                                  child: VerticalDivider(
                                    width: 4,
                                    thickness: 4,
                                    color: lightBlue,
                                  )),
                              const SizedBox(
                                width: 10,
                              ),
                              Image.asset(
                                'assets/images/logout.png',
                                height: 20,
                              ),
                              const SizedBox(
                                width: 20,
                              ),
                              Text(
                                'Logout',
                                style: GoogleFonts.poppins(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    color: blackColor),
                              ),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          appBar: _currentIndex == 0
              ? AppBar(
                  backgroundColor: Colors.transparent,
                  elevation: 0,
                  leading: IconButton(
                    onPressed: () {
                      scaffoldKey.currentState?.openDrawer();
                    },
                    icon: Image.asset(
                      'assets/images/menu.png',
                      scale: 2.5,
                    ),
                  ),
                  title: Text(
                    'Nayi Urja',
                    style: GoogleFonts.poppins(color: blackColor),
                  ),
                  centerTitle: true,
                  // actions: [
                  //   Icon(
                  //     Icons.search,
                  //     size: 25,
                  //     color: blackColor,
                  //   ),
                  //   const SizedBox(
                  //     width: 10,
                  //   ),
                  //   Icon(
                  //     Icons.person,
                  //     size: 25,
                  //     color: blackColor,
                  //   ),
                  //   const SizedBox(
                  //     width: 10,
                  //   )
                  // ],
                )
              : null,
          body: _currentIndex == 0
              ? Column(
                  children: [
                    Column(children: [
                      CarouselSlider(
                        items: allgetBannersList.map((data) {
                          return Builder(
                            builder: (BuildContext context) {
                              return Container(
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: NetworkImage(data
                                        .bannerImage!), // Use NetworkImage for URLs
                                    fit: BoxFit.fill,
                                  ),
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                width: MediaQuery.of(context).size.width,
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 20.0),
                              );
                            },
                          );
                        }).toList(),
                        options: CarouselOptions(
                          height: 140,
                          autoPlay: true,
                          enlargeCenterPage: true,
                          viewportFraction: 1.0,
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      )
                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.center,
                      //   children: imageList.asMap().entries.map((entry) {
                      //     int index = entry.key;
                      //     print(index);
                      //     return Container(
                      //       width: 18.0,
                      //       height: 4.0,
                      //       margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                      //       decoration: BoxDecoration(
                      //         shape: BoxShape.rectangle,
                      //         color: _carouselcurrentIndex == index ? Colors.blue : Colors.grey,
                      //       ),
                      //     );
                      //   }).toList(),
                      // ),
                    ]),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(
                              height: 5,
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              child: Row(
                                children: [
                                  Text(
                                    AppConstants.discoverTest,
                                    style: GoogleFonts.poppins(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                      height: 1.25,
                                      color: blackColor,
                                    ),
                                  ),
                                  Spacer(),
                                  InkWell(
                                    onTap: () {
                                      Navigator.pushNamed(
                                          context, RoutesName.testSeries);
                                    },
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Text(
                                          AppConstants.viewAll,
                                          style: GoogleFonts.poppins(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w600,
                                            height: 1.25,
                                            color: lightRed,
                                          ),
                                        ),
                                        Icon(
                                          Icons.arrow_forward,
                                          color: lightRed,
                                          size: 20,
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 10),
                              child: Column(
                                children: [
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  SizedBox(
                                      height: 300,
                                      child: ListView.builder(
                                        shrinkWrap: true,
                                        itemCount: courseTestSeriesList.length,
                                        scrollDirection: Axis.horizontal,
                                        itemBuilder: (context, index) {
                                          CourseTestSeries testSeries =
                                              courseTestSeriesList[index];
                                          return InkWell(
                                            onTap: (){
                                              Navigator.push(context, MaterialPageRoute(builder: (context) => TestSeriesDetails(testSeries.testType.toString()),));
                                            },
                                            child: Container(
                                                margin: EdgeInsets.all(10),
                                                width: 200,
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(10),
                                                    color: whiteColor),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Image.asset(
                                                      testSeries.image!,
                                                      fit: BoxFit.cover,
                                                      height: 130,
                                                      errorBuilder:
                                                          (BuildContext context,
                                                              Object exception,
                                                              StackTrace?
                                                                  stackTrace) {
                                                        // This function is called when the image fails to load
                                                        return Image.asset(
                                                          'assets/images/discoverbanner.png',
                                                          fit: BoxFit.cover,
                                                          height: 140,
                                                        ); // Display the fallback asset image
                                                      },
                                                    ),
                                                    Container(
                                                      decoration: BoxDecoration(
                                                          color: whiteColor,
                                                          borderRadius:
                                                              BorderRadius.only(
                                                                  bottomLeft: Radius
                                                                      .circular(
                                                                          10),
                                                                  bottomRight: Radius
                                                                      .circular(
                                                                          10))),
                                                      padding: EdgeInsets.all(10),
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          Text(
                                                            testSeries.testType.toString().length > 24 ? '${testSeries.testType.toString().substring(0, 24)}...' : testSeries.testType.toString(),
                                                            style: GoogleFonts
                                                                .poppins(
                                                              fontSize: 14,
                                                              fontWeight:
                                                                  FontWeight.w500,
                                                              height: 1.25,
                                                              color: lightRed,
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                            height: 10,
                                                          ),
                                                          Text(
                                                            testSeries.description.toString().replaceAll("\n", "/").length > 50 ? '${testSeries.description.toString().replaceAll("\n", "/").substring(0, 50)}...' : testSeries.description.toString().replaceAll("\n", "/"),
                                                            style: GoogleFonts
                                                                .poppins(
                                                              fontSize: 12,
                                                              fontWeight:
                                                                  FontWeight.w400,
                                                              height: 1.4,
                                                              color: blackColor
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            height: 5,
                                                          ),
                                                          Row(
                                                            children: [
                                                              Text(
                                                                AppConstants
                                                                    .nextPattern,
                                                                style: GoogleFonts
                                                                    .poppins(
                                                                  fontSize: 12,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500,
                                                                  height: 1.25,
                                                                  color:
                                                                      textField,
                                                                ),
                                                              ),
                                                              Spacer(),
                                                              Container(
                                                                decoration: BoxDecoration(
                                                                    color:
                                                                        lightSky,
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .circular(
                                                                                10)),
                                                                padding:
                                                                    EdgeInsets
                                                                        .all(10),
                                                                child: Row(
                                                                    children: [
                                                                      Text(
                                                                        AppConstants
                                                                            .rupeesSymbol,
                                                                        style: GoogleFonts
                                                                            .poppins(
                                                                          fontSize:
                                                                              12,
                                                                          fontWeight:
                                                                              FontWeight.w500,
                                                                          height:
                                                                              1.25,
                                                                          color:
                                                                              lightBlue,
                                                                        ),
                                                                      ),
                                                                      SizedBox(
                                                                        width: 5,
                                                                      ),
                                                                      Text(
                                                                        testSeries.price.toString(),
                                                                        style: GoogleFonts
                                                                            .poppins(
                                                                          fontSize:
                                                                              12,
                                                                          fontWeight:
                                                                              FontWeight.w500,
                                                                          height:
                                                                              1.25,
                                                                          color:
                                                                              lightBlue,
                                                                        ),
                                                                      ),
                                                                    ]),
                                                              )
                                                            ],
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                )),
                                          );
                                        },
                                      )),
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 20),
                              child: Text(
                                AppConstants.coursesOfferred,
                                style: GoogleFonts.poppins(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w600,
                                  height: 1.25,
                                  color: blackColor,
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              alignment: Alignment.centerLeft,
                              child: ListView.builder(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: courseOfferedList.length,
                                scrollDirection: Axis.vertical,
                                itemBuilder: (context, index) {
                                  CourseOffered course =
                                      courseOfferedList[index];
                                  String? bookImage =
                                      courseOfferedList[index].bookImage;

                                  return Container(
                                      decoration: BoxDecoration(
                                          color: whiteColor,
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      margin: EdgeInsets.all(10),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Expanded(
                                            flex: 1,
                                            child: ClipRRect(
                                              borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(10),
                                                  bottomLeft:
                                                      Radius.circular(10)),
                                              child: Image.asset(
                                                course.bookImage!,
                                                fit: BoxFit.cover,
                                                height: 120,
                                                errorBuilder: (BuildContext
                                                        context,
                                                    Object exception,
                                                    StackTrace? stackTrace) {
                                                  // This function is called when the image fails to load
                                                  return Image.asset(
                                                    'assets/images/discoverbanner.png',
                                                    fit: BoxFit.cover,
                                                    height: 120,
                                                  ); // Display the fallback asset image
                                                },
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            flex: 2,
                                            child: Container(
                                              height: 120,
                                              padding: EdgeInsets.all(10),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    children: [
                                                      Image.asset(
                                                        'assets/images/off.png',
                                                        height: 16,
                                                      ),
                                                      SizedBox(
                                                        width: 5,
                                                      ),
                                                      Text(
                                                        course.offerPercent
                                                            .toString(),
                                                        style:
                                                            GoogleFonts.poppins(
                                                          fontSize: 12,
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          height: 1.25,
                                                          color: subText,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  Text(
                                                    course.bookName.toString(),
                                                    style: GoogleFonts.poppins(
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      color: blackColor,
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  Row(
                                                    children: [
                                                      InkWell(onTap:  (){
                                                        if((course.price??'0').isNotEmpty){
                                                          Navigator.push(context, MaterialPageRoute(builder: (context) => BuyNowPage(course.price.toString(),course.courseId.toString(), course.bookDes.toString(),course.bookImage.toString()),));
                                                        }else{
                                                          Fluttertoast.showToast(msg: 'Invalid amount');
                                                        }
                                                        print("bookprice ${course.price.toString()}");
                                                      },
                                                        child: Container(
                                                          decoration: BoxDecoration(
                                                              color: lightSky,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          10)),
                                                          padding: EdgeInsets
                                                              .symmetric(
                                                                  vertical: 10,
                                                                  horizontal: 20),
                                                          child: Text(
                                                            AppConstants.buy,
                                                            style: GoogleFonts
                                                                .poppins(
                                                              fontSize: 12,
                                                              fontWeight:
                                                                  FontWeight.w500,
                                                              height: 1.25,
                                                              color: lightBlue,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        width: 20,
                                                      ),
                                                      InkWell(
                                                        onTap: (){
                                                          // Navigator.pushNamed(context, RoutesName.courseDetail);
                                                          Navigator.push(context, MaterialPageRoute(builder: (context) => CourseDetails(course.bookName!),));
                                                        },
                                                        child: Container(
                                                          decoration: BoxDecoration(
                                                              color: brightRed,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          10)),
                                                          padding: EdgeInsets
                                                              .symmetric(
                                                                  vertical: 10,
                                                                  horizontal: 20),
                                                          child: Text(
                                                            AppConstants.tryNow,
                                                            style: GoogleFonts
                                                                .poppins(
                                                              fontSize: 12,
                                                              fontWeight:
                                                                  FontWeight.w500,
                                                              height: 1.25,
                                                              color: lightRed,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ));
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                )
              : _currentIndex == 1
                  ? const Report()
                  : _currentIndex == 2
                      ? const Offer()
                      : const TestSeries(),
          bottomNavigationBar: Container(
            color: Colors.white,
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: BottomNavigationBar(
                backgroundColor: Colors.transparent,
                selectedIconTheme: IconThemeData(color: lightBlue),
                type: BottomNavigationBarType.fixed,
                currentIndex: _currentIndex,
                unselectedItemColor: textField,
                selectedItemColor: lightBlue,
                onTap: _onItemTapped,
                elevation: 0,
                items: const [
                  BottomNavigationBarItem(
                      icon: ImageIcon(AssetImage('assets/images/home.png')),
                      label: 'Home'),
                  BottomNavigationBarItem(
                      icon: ImageIcon(AssetImage('assets/images/report.png')),
                      label: 'Report'),
                  BottomNavigationBarItem(
                      icon: ImageIcon(AssetImage('assets/images/offers.png')),
                      label: 'Offer'),
                  BottomNavigationBarItem(
                      icon:
                          ImageIcon(AssetImage('assets/images/testSeries.png')),
                      label: 'Test Series'),
                ]),
          ),
        ),
      ),
    );
  }

  List<BannerResult> allgetBannersList = [];

  Future<void> getBannersList() async {
    AppLoaderProgress.showLoader(context);
    final response = await http.post(
      Uri.parse(UtilsMethod.baseUrl),
      body: {
        "api_name": "getBannersList",
      },
    );
    if (response.statusCode == 200) {
      allgetBannersList =
          BannerModels.fromJson(jsonDecode(response.body)).resonse!.result!;
      setState(() {});
    }

    AppLoaderProgress.hideLoader(context);
  }

  List<CourseOffered> courseOfferedList = [];
  List<CourseTestSeries> courseTestSeriesList = [];
  List<CourseVersions> courseVersionsList = [];

  Future<void> getCourseList() async {
    AppLoaderProgress.showLoader(context);
    String clientId = await AppPrefrence.getString("id");
    String teacherId = await AppPrefrence.getString('teacher_id');

    final response = await http.post(
      Uri.parse(UtilsMethod.baseUrl),
      body: {
        "api_name": "getAllCourses",
        "client_id": clientId,
        "teacher_id": teacherId,
      },
    );
    if (response.statusCode == 200) {
      var responses = jsonDecode(response.body);

      courseOfferedList =
          CourseModels.fromJson(responses).resonse!.result!.offered!;
      courseTestSeriesList =
          CourseModels.fromJson(responses).resonse!.result!.testSeries!;
      courseVersionsList =
          CourseModels.fromJson(responses).resonse!.result!.versions!;
      for (CourseVersions courseVersion in courseVersionsList) {
        if (courseVersion.appName == "CosmoLearn") {
          _initPackageInfo(int.parse(courseVersion.appVersion!));
          break;
        }

      }
      setState(() {});
    }

    AppLoaderProgress.hideLoader(context);
  }


}
