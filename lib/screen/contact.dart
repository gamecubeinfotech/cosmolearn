import 'package:cosmolearn/appUtilities/app_constants.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../appUtilities/app_colors.dart';

class Contact extends StatefulWidget {
  const Contact({super.key});

  @override
  State<Contact> createState() => _ContactState();
}

class _ContactState extends State<Contact> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(backgroundColor: mainColor,
      appBar: AppBar(
        iconTheme: const IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text(
          AppConstants.contactUs,
          style: GoogleFonts.poppins(
              fontSize: 16, fontWeight: FontWeight.w600, color: blackColor),
        ),
      ),
      body: Column(
        children: [
          Container(
            height: 140,
            decoration: BoxDecoration(
                image: const DecorationImage(
                  image: AssetImage('assets/images/banner.png'),
                  fit: BoxFit.fill,
                ),
                borderRadius: BorderRadius.circular(15)),
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.symmetric(horizontal: 20),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 10,horizontal: 20),
            decoration: BoxDecoration(
              color: whiteColor,
              borderRadius: BorderRadius.circular(10),
            ),
            padding: const EdgeInsets.all(10),
            child: Row(
              children: [
                Container(
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10),border: Border.all(color: lightSky)),
                  padding: const EdgeInsets.all(15),
                  child: Icon(
                    Icons.email_rounded,
                    color: lightBlue,
                  ),
                ),
                const SizedBox(width: 10,),
                Column(crossAxisAlignment: CrossAxisAlignment.start,mainAxisAlignment: MainAxisAlignment.spaceEvenly,children: [
                  Text(
                    AppConstants.supportMail,
                    style: GoogleFonts.poppins(
                        fontSize: 16, fontWeight: FontWeight.w600, color: blackColor),
                  ),
                  const SizedBox(height: 10,),
                  Text(
                    AppConstants.cosmolearnMail,
                    style: GoogleFonts.poppins(
                        fontSize: 13, fontWeight: FontWeight.w500, color: textField),
                  ),
                ],),

              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10,horizontal: 20),
            decoration: BoxDecoration(
              color: whiteColor,
              borderRadius: BorderRadius.circular(10),
            ),
            padding: const EdgeInsets.all(10),
            child: Row(
              children: [
                Container(
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10),border: Border.all(color: lightSky)),
                  padding: const EdgeInsets.all(15),
                  child: Icon(
                    Icons.email_rounded,
                    color: lightBlue,
                  ),
                ),
                const SizedBox(width: 10,),
                Column(crossAxisAlignment: CrossAxisAlignment.start,mainAxisAlignment: MainAxisAlignment.spaceEvenly,children: [
                  Text(
                    AppConstants.saleMail,
                    style: GoogleFonts.poppins(
                        fontSize: 16, fontWeight: FontWeight.w600, color: blackColor),
                  ),
                  const SizedBox(height: 10,),
                  Text(
                    AppConstants.cosmolearnSalesMail,
                    style: GoogleFonts.poppins(
                        fontSize: 13, fontWeight: FontWeight.w500, color: textField),
                  ),
                ],),

              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10,horizontal: 20),
            decoration: BoxDecoration(
              color: whiteColor,
              borderRadius: BorderRadius.circular(10),
            ),
            padding: const EdgeInsets.all(10),
            child: Row(
              children: [
                Container(
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10),border: Border.all(color: lightSky)),
                  padding: const EdgeInsets.all(15),
                  child: Icon(
                    Icons.email_rounded,
                    color: lightBlue,
                  ),
                ),
                const SizedBox(width: 10,),
                Column(crossAxisAlignment: CrossAxisAlignment.start,mainAxisAlignment: MainAxisAlignment.spaceEvenly,children: [
                  Text(
                    AppConstants.contactUs,
                    style: GoogleFonts.poppins(
                        fontSize: 16, fontWeight: FontWeight.w600, color: blackColor),
                  ),
                  const SizedBox(height: 10,),
                  Text(
                    AppConstants.cosmolearnContactMail,
                    style: GoogleFonts.poppins(
                        fontSize: 13, fontWeight: FontWeight.w500, color: textField),
                  ),
                ],),

              ],
            ),
          ),
        ],
      ),
    );
  }
}
