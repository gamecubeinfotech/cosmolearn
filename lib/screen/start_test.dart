import 'dart:convert';

import 'package:cosmolearn/screen/result_screen.dart';
import 'package:cosmolearn/screen/review.dart';
import 'package:cosmolearn/view_models/question_list_models.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:custom_timer/custom_timer.dart';
import '../appUtilities/AppPrefrences.dart';
import '../appUtilities/app_circular_loader.dart';
import '../appUtilities/app_colors.dart';
import '../appUtilities/app_constants.dart';
import 'package:http/http.dart' as http;

import '../appUtilities/utils.dart';
import '../view_models/add_question_list.dart';
import '../view_models/result_models.dart';

class StartTest extends StatefulWidget {
  String? testName;
  String? ttname;
  String? testTypeId;
  String? time;

  StartTest(this.testName, this.ttname, this.testTypeId, this.time, {super.key});

  @override
  State<StartTest> createState() => _StartTestState();
}

class _StartTestState extends State<StartTest>
    with SingleTickerProviderStateMixin {
  int currentQuestionIndex = 0;
  int myIndex = 0;
  String? clientId;

  late CustomTimerController _mController;

  void selectAnswer(String answerMarked) {
    setState(() {
      if (myIndex < getQuestionData.length - 1) {
        addData('ATTEMPTED', answerMarked);
        myIndex++;
      }
    });
  }

  void skipQuestion() {
    setState(() {
      if (myIndex < getQuestionData.length - 1) {
        addData('NOT_ATTEMPTED', '');
        myIndex++;
      }
    });
  }

  void addData(String check, String answerMarked) async {
    AddQuestionModels addQuestion = AddQuestionModels();
    clientId = await AppPrefrence.getString("id");
    addQuestion.book_id = '0';
    addQuestion.q_name = getQuestionData[myIndex].question;
    addQuestion.answer_marked = answerMarked;
    addQuestion.check_status =
        answerMarked == getQuestionData[myIndex].correctAnswer
            ? "true"
            : "false";
    addQuestion.chapter_id = '0';
    addQuestion.client_id = clientId;
    addQuestion.correct_answer = getQuestionData[myIndex].correctAnswer;
    addQuestion.flag = check;
    addQuestion.is_submit = "No";
    addQuestion.question_number = getQuestionData[myIndex].qNo;
    addQuestion.time_taken_to_solve = "0s"
        "";
    addQuestion.ttname_id = getQuestionData[myIndex].ttnameId;
    addQuestion.user_mobile =
        (await AppPrefrence.getString("mobile")).toString();
    UtilsMethod.addQuestionList.add(addQuestion);
  }

  void reviewQuestions() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ReviewScreen(
          chapterId: getQuestionData[myIndex].ttnameId,
          time: getQuestionData[myIndex].timeTakenToSolve,
          testTypeId: widget.testTypeId,
          id: getQuestionData[myIndex].id,
          position: 0,
        ),
      ),
    );
  }

  @override
  void initState() {
    String time = widget.time!.split(" min")[0];
    _mController = CustomTimerController(
        vsync: this,
        begin: Duration(
          minutes: int.parse(time),
        ),
        end: const Duration(seconds: 0),
        initialState: CustomTimerState.reset,
        interval: CustomTimerInterval.milliseconds);
    _mController.start();
    Future.delayed(Duration.zero, () {
      testTypeSeriesData();
    });
    super.initState();
  }

  showSubmitPopup(context) {
    return showDialog(
        context: context,
        builder: (contexts) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            actions: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                    child: Text(
                      "Do you want to Submit this Test?",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.lexend(
                          color: primaryColor,
                          fontWeight: FontWeight.w500,
                          fontSize: 16),
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        height: 30,
                        width: 72,
                        decoration: BoxDecoration(
                            color: brightRed,
                            borderRadius: BorderRadius.circular(100)),
                        child: InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Center(
                            child: Text(
                              "No",
                              style: GoogleFonts.poppins(
                                  color: lightRed,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(width: 10),
                      Container(
                        height: 30,
                        width: 72,
                        decoration: BoxDecoration(
                            color: lightSky,
                            borderRadius: BorderRadius.circular(100)),
                        child: InkWell(
                          onTap: () async {
                            Navigator.pop(context);
                            if (UtilsMethod.addQuestionList.isNotEmpty) {
                              submitTestData(context);
                            } else {
                              Fluttertoast.showToast(
                                  msg: 'Please select at lease on question');
                            }
                          },
                          child: Center(
                            child: Text(
                              "Yes",
                              style: GoogleFonts.poppins(
                                  color: lightBlue,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 14),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(width: 20),
                    ],
                  ),
                ],
              ),
            ],
            actionsPadding: const EdgeInsets.only(bottom: 15),
          );
        });
  }

  showExitPopup() {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            actions: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                    child: Text(
                      "Sure do you want to exit?",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.lexend(
                          color: primaryColor,
                          fontWeight: FontWeight.w500,
                          fontSize: 16),
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        height: 30,
                        width: 72,
                        decoration: BoxDecoration(
                            color: lightSky,
                            borderRadius: BorderRadius.circular(100)),
                        child: InkWell(
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.pop(context);
                          },
                          child: Center(
                            child: Text(
                              "Yes",
                              style: GoogleFonts.poppins(
                                  color: lightBlue,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 14),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(width: 10),
                      Container(
                        height: 30,
                        width: 72,
                        decoration: BoxDecoration(
                            color: brightRed,
                            borderRadius: BorderRadius.circular(100)),
                        child: InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Center(
                            child: Text(
                              "No",
                              style: GoogleFonts.poppins(
                                  color: lightRed,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(width: 20),
                    ],
                  ),
                ],
              ),
            ],
            actionsPadding: const EdgeInsets.only(bottom: 15),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return  showExitPopup();
      },
      child: Scaffold(
        backgroundColor: mainColor,
        appBar: AppBar(
          iconTheme: const IconThemeData(
            color: Colors.black, //change your color here
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
          title: Text(
            widget.testName!,
            style: GoogleFonts.poppins(
                fontSize: 16, fontWeight: FontWeight.w600, color: blackColor),
          ),
          actions: [
            Row(
              children: [
                Text(
                  'Time: ',
                  style: GoogleFonts.poppins(
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: blackColor),
                ),
                const SizedBox(
                  width: 5,
                ),
                CustomTimer(
                  controller: _mController,
                  builder: (CustomTimerState, remaining) {
                    return Text(
                      "${remaining.hours}h : ${remaining.minutes}m : ${remaining.seconds}s",
                      style: GoogleFonts.poppins(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          color: blackColor),
                    );
                  },
                ),
                const SizedBox(
                  width: 10,
                ),
              ],
            ),
          ],
        ),
        body: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            SizedBox(
              height: MediaQuery.of(context).size.height,
              child: SingleChildScrollView(
                child: Container(
                  padding: const EdgeInsets.only(bottom: 60),
                  // height: MediaQuery.of(context).size.height,
                  margin: const EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 10,
                      ),
                      ListView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: 1,
                        itemBuilder: (context, index) {
                          if (myIndex >= 0 && myIndex < getQuestionData.length) {
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  getQuestionData[myIndex].question.toString(),
                                  style: const TextStyle(
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.w600),
                                  textAlign: TextAlign.start,
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                Image.network(
                                  getQuestionData[myIndex]
                                      .questionImage
                                      .toString(),
                                  fit: BoxFit.fill,
                                  width: MediaQuery.of(context).size.width,
                                  height: 110,
                                  errorBuilder: (context, error, stackTrace) {
                                    return Container();
                                  },
                                ),
                              ],
                            );
                          } else {
                            return Container();
                          }
                        },
                      ),
                      const SizedBox(height: 15.0),
                      ListView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: 1,
                        itemBuilder: (context, index) {
                          if (myIndex >= 0 && myIndex < getQuestionData.length) {
                            return Column(
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    selectAnswer("A");
                                  },
                                  child: Container(
                                    margin:
                                        const EdgeInsets.symmetric(vertical: 8),
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 8),
                                    decoration: BoxDecoration(
                                      boxShadow: [
                                        BoxShadow(
                                          color: borderColor,
                                          blurRadius: 4,
                                          offset: Offset(4, 8), // Shadow position
                                        ),
                                      ],
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                    ),
                                    child: Column(
                                      children: [
                                        Row(
                                          children: [
                                            Container(
                                              padding: const EdgeInsets.all(10),
                                              decoration: BoxDecoration(
                                                color: lightSky,
                                                shape: BoxShape.circle,
                                              ),
                                              child: Text((index + 1).toString()),
                                            ),
                                            const SizedBox(width: 10),
                                            Expanded(
                                              child: Text(
                                                getQuestionData[myIndex]
                                                    .optionA
                                                    .toString(),
                                                textAlign: TextAlign.start,
                                                style:
                                                    TextStyle(color: blackColor),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Image.network(
                                          getQuestionData[myIndex]
                                              .optionAImage
                                              .toString(),
                                          fit: BoxFit.fill,
                                          errorBuilder:
                                              (context, error, stackTrace) {
                                            return Container();
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    selectAnswer("B");
                                  },
                                  child: Container(
                                    margin:
                                        const EdgeInsets.symmetric(vertical: 8),
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 8),
                                    decoration: BoxDecoration(
                                      boxShadow: [
                                        BoxShadow(
                                          color: borderColor,
                                          blurRadius: 4,
                                          offset: const Offset(4, 8), // Shadow position
                                        ),
                                      ],
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                    ),
                                    child: Column(
                                      children: [
                                        Row(
                                          children: [
                                            Container(
                                              padding: const EdgeInsets.all(10),
                                              decoration: BoxDecoration(
                                                color: lightSky,
                                                shape: BoxShape.circle,
                                              ),
                                              child: Text((index + 2).toString()),
                                            ),
                                            const SizedBox(width: 10),
                                            Expanded(
                                              child: Text(
                                                getQuestionData[myIndex]
                                                    .optionB
                                                    .toString(),
                                                textAlign: TextAlign.start,
                                                style:
                                                    TextStyle(color: blackColor),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Image.network(
                                          getQuestionData[myIndex]
                                              .optionBImage
                                              .toString(),
                                          fit: BoxFit.fill,
                                          errorBuilder:
                                              (context, error, stackTrace) {
                                            return Container();
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    selectAnswer("C");
                                  },
                                  child: Container(
                                    margin:
                                        const EdgeInsets.symmetric(vertical: 8),
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 8),
                                    decoration: BoxDecoration(
                                      boxShadow: [
                                        BoxShadow(
                                          color: borderColor,
                                          blurRadius: 4,
                                          offset: const Offset(4, 8), // Shadow position
                                        ),
                                      ],
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                    ),
                                    child: Column(
                                      children: [
                                        Row(
                                          children: [
                                            Container(
                                              padding: const EdgeInsets.all(10),
                                              decoration: BoxDecoration(
                                                color: lightSky,
                                                shape: BoxShape.circle,
                                              ),
                                              child: Text((index + 3).toString()),
                                            ),
                                            const SizedBox(width: 10),
                                            Expanded(
                                              child: Text(
                                                getQuestionData[myIndex]
                                                    .optionC
                                                    .toString(),
                                                textAlign: TextAlign.start,
                                                style:
                                                    TextStyle(color: blackColor),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Image.network(
                                          getQuestionData[myIndex]
                                              .optionCImage
                                              .toString(),
                                          fit: BoxFit.fill,
                                          errorBuilder:
                                              (context, error, stackTrace) {
                                            return Container();
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    selectAnswer("D");
                                  },
                                  child: Container(
                                    margin:
                                        const EdgeInsets.symmetric(vertical: 8),
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 8),
                                    decoration: BoxDecoration(
                                      boxShadow: [
                                        BoxShadow(
                                          color: borderColor,
                                          blurRadius: 4,
                                          offset: const Offset(4, 8), // Shadow position
                                        ),
                                      ],
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                    ),
                                    child: Column(
                                      children: [
                                        Row(
                                          children: [
                                            Container(
                                              padding: const EdgeInsets.all(10),
                                              decoration: BoxDecoration(
                                                color: lightSky,
                                                shape: BoxShape.circle,
                                              ),
                                              child: Text((index + 4).toString()),
                                            ),
                                            const SizedBox(width: 10),
                                            Expanded(
                                              child: Text(
                                                getQuestionData[myIndex]
                                                    .optionD
                                                    .toString(),
                                                textAlign: TextAlign.start,
                                                style:
                                                    TextStyle(color: blackColor),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Image.network(
                                          getQuestionData[myIndex]
                                              .optionDImage
                                              .toString(),
                                          fit: BoxFit.fill,
                                          errorBuilder:
                                              (context, error, stackTrace) {
                                            return Container();
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            );
                          } else {
                            return Container();
                          }
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              alignment: Alignment.bottomCenter,
              padding: const EdgeInsets.only(bottom: 10, top: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: InkWell(
                      onTap: reviewQuestions,
                      child: Container(
                        alignment: Alignment.center,
                        margin: const EdgeInsets.only(right: 5, left: 5),
                        height: 40,
                        decoration: BoxDecoration(
                          color: lightBlue,
                          borderRadius: BorderRadius.circular(50),
                        ),
                        child: Text(
                          AppConstants.review,
                          style: GoogleFonts.poppins(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            height: 1.4,
                            color: whiteColor,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      onTap: skipQuestion,
                      child: Container(
                        alignment: Alignment.center,
                        margin: const EdgeInsets.only(right: 5, left: 5),
                        height: 40,
                        decoration: BoxDecoration(
                          color: lightBlue,
                          borderRadius: BorderRadius.circular(50),
                        ),
                        child: Text(
                          AppConstants.skip,
                          style: GoogleFonts.poppins(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            height: 1.4,
                            color: whiteColor,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        showSubmitPopup(context);
                      },
                      child: Container(
                        alignment: Alignment.center,
                        margin: const EdgeInsets.only(right: 5, left: 5),
                        height: 40,
                        decoration: BoxDecoration(
                          color: lightBlue,
                          borderRadius: BorderRadius.circular(50),
                        ),
                        child: Text(
                          AppConstants.submitButton,
                          style: GoogleFonts.poppins(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            height: 1.4,
                            color: whiteColor,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<QuestionListResult> getQuestionData = [];
  Future<void> testTypeSeriesData() async {
    AppLoaderProgress.showLoader(context);
    clientId = await AppPrefrence.getString("id");

    final response = await http.post(Uri.parse(UtilsMethod.baseUrl), body: {
      "api_name": "getTestQuestionBank",
      "client_id": clientId,
      "ttname_id": widget.ttname,
    });
    if (response.statusCode == 200) {
      var responses = jsonDecode(response.body); 

      getQuestionData = QuestionListModels.fromJson(responses).resonse!.result!;
      UtilsMethod.addQuestionList.clear();
      setState(() {});
    }
    AppLoaderProgress.hideLoader(context);
  }

  Future<void> submitTestData(context) async {
    AppLoaderProgress.showLoader(context);
    String clientId = await AppPrefrence.getString("id");
    final response = await http.post(Uri.parse(UtilsMethod.baseUrl), body: {
      "api_name": "submitTest",
      "data": jsonEncode(UtilsMethod.addQuestionList),
      "client_id": clientId,
      "ttname_id": getQuestionData[myIndex].ttnameId,
      "isTestSeries": "Yes",
      "test_type_id": getQuestionData[myIndex].id,
    });

    AppLoaderProgress.hideLoader(context);
    if (response.statusCode == 200) {
      var responses = jsonDecode(response.body);
      SubmitResultResult getResultData =
          SubmitResultModels.fromJson(responses).resonse!.result!;
      _mController.finish();
      Navigator.pop(context);
      Navigator.pop(context);
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => resultPage(getResultData),
          ));
    }
  }
}
